
if (NOT DEFINED GHOST_REFLECT_BUILD)
  add_subdirectory(reflect-interface)
endif()


add_executable(ghost-reflect-test0
  test0.cpp
  test0.hpp
  stupid_include_dir/stupid_include.hpp)

target_compile_features(ghost-reflect-test0 PRIVATE cxx_std_17)
target_link_libraries(ghost-reflect-test0 PUBLIC reflect_interface)
target_include_directories(ghost-reflect-test0 PUBLIC stupid_include_dir)
