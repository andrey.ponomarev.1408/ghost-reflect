/*!
 * \file      test0.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include <iostream>
#include "test0.hpp"

#include <boost/type_index.hpp>

void foo() {
  math::vec3 v;
}

//template<typename... T>
//struct pretty_printer;

//template<typename T>
//struct pretty_printer<T>
//{
//  friend std::ostream& operator<<(std::ostream &os, const pretty_printer&) {
//    return os << boost::typeindex::type_id_with_cvr<T>().pretty_name();
//  }
//};

//template<typename T, typename... Targs>
//struct pretty_printer<T, Targs...>
//{
//  friend std::ostream& operator<<(std::ostream &os, const pretty_printer&) {
//    return os << pretty_printer<T>() << pretty_printer<Targs...>();
//  }
//};

//template<typename Fn, typename... Targs>
//struct fn_info_printer
//{
//  friend std::ostream& operator<<(std::ostream &os, const fn_info_printer&) {
//    os << "[Type]: ";
//    os << pretty_printer<Fn>();
//    os << "\n[Args]: ";
//    os << pretty_printer<Targs...>();
//    os << "\n[Return type]: ";
//    os << pretty_printer<std::invoke_result_t<Fn, Targs...>>();
//    os << "\n--------------------------------------\n";
//    return os;
//  }  
//};

//void my_stupid_test() {
//  struct foo
//  {
//    int& bar0() {return i;}
//    const int& bar1() const {return i;}
    
//    int i;
//  };
  
//  std::function<const int&(const foo&)> fn0 = [](const foo& v)->const int& {return v.i;};
//  std::function<int&(foo&)> fn1 = [](foo& v)->int& {return v.i;};

    
//  auto m0 = meta::make_method(fn0);
//  auto m1 = meta::make_method(fn1);
//  auto m2 = meta::make_method(&foo::bar0);
//  auto m3 = meta::make_method(&foo::bar1);
  
//  auto get0 = meta::make_getter(fn0);
//  auto get1 = meta::make_getter(fn1);
//  auto get2 = meta::make_getter(&foo::bar0);
//  auto get3 = meta::make_getter(&foo::bar1);
//  auto get4 = meta::make_getter(&foo::i);
  
//  auto set0 = meta::make_setter(&foo::i);
  
//  std::cout << "Methods:\n";
//  fn_info_printer<decltype(m0), const foo&>();
//  std::cout << fn_info_printer<decltype(m0), const foo&>();
//  std::cout << fn_info_printer<decltype(m1), foo&>();
//  std::cout << fn_info_printer<decltype(m2), foo&>();
//  std::cout << fn_info_printer<decltype(m3), const foo&>();

  
//  std::cout << "\nGetters:\n";
//  std::cout << fn_info_printer<decltype(get0), const foo&>();
//  std::cout << fn_info_printer<decltype(get1), foo&>();
//  std::cout << fn_info_printer<decltype(get2), foo&>();
//  std::cout << fn_info_printer<decltype(get3), const foo&>();
//  std::cout << fn_info_printer<decltype(get3), const foo&>();
  
  
//  std::cout << "Setters:\n";
//  std::cout << fn_info_printer<decltype(set0), foo&, int>();
//}

int main()
{
  using namespace std;
  
//  my_stupid_test();
  
//  struct foo
//  {
//    int& GetI() {
//      return i;
//    }
//    const int& GetI() const {
//      return i;
//    }
//    void SetI(int i) {
//      return;
//    }
//    int SetJ(float j) {
//      return (int)(j);
//    }
    
//    int i;
//  }obj;
  
//  auto i_getter = meta::make_non_const_getter(&foo::GetI);
//  auto i_c_getter = meta::make_const_getter(&foo::GetI);
//  auto i_direct_getter = meta::make_getter(&foo::i);
  
//  auto val1 = i_getter(obj);
//  auto val2 = i_c_getter(obj);
//  auto val3 = i_direct_getter(obj);
  
  
  
  
  return 0;
}
