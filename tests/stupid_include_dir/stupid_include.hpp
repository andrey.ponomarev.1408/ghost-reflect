/*!
 * \file      stupid_include.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include <ghost-reflect/ghost-reflect.hpp>

struct attrs() struct_from_stupid_include
{
  
};
