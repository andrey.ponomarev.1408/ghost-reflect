/*!
 * \file      test0.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include <type_traits>
#include <utility>
#include <functional>
#include <string_view>
#include <boost/hana.hpp>
#include <boost/hana/integral_constant.hpp>
#include <boost/preprocessor.hpp>

#define c_str(x) BOOST_HANA_STRING(x)


using namespace std::literals;

namespace hana = boost::hana;
using namespace hana::literals;


namespace description {
template <typename T> struct fn_to_method;

template <typename Return, typename... Args, typename Class>
struct fn_to_method<Return(Class *, Args...)> {
  static_assert(std::is_class_v<Class>, "Class should be a class, dough");

  using type = Return (Class::*)(Args...);
};

template <typename Return, typename... Args, typename Class>
struct fn_to_method<Return(const Class *, Args...)> {
  static_assert(std::is_class_v<Class>, "Class should be a class, dough");

  using type = Return (Class::*)(Args...) const;
};

template <typename Return, typename... Args, typename Class>
struct fn_to_method<Return(Class &, Args...)> {
  static_assert(std::is_class_v<Class>, "Class should be a class, dough");

  using type = Return (Class::*)(Args...);
};

template <typename Return, typename... Args, typename Class>
struct fn_to_method<Return(const Class &, Args...)> {
  static_assert(std::is_class_v<Class>, "Class should be a class, dough");

  using type = Return (Class::*)(Args...) const;
};

}


template <typename T>
static constexpr T* null = 0;



template <typename T>
[[nodiscard]]
constexpr T *cast_fn(T *fn) {
  static_assert(std::is_function_v<T>, "T should be a function");
  return fn;
}

template <typename T>
[[nodiscard]]
constexpr typename description::fn_to_method<T>::type
cast_fn(typename description::fn_to_method<T>::type fn) {
  return fn;
}


namespace [[reflect]] math {

  struct simple_struct {
    int i;
    int j;
    static int static_var;
  };

  extern int some_global_var;

  class interesting_class {
  public:
    int getI() const;
    int &getI();
    void setI(int val);

    static void static_method();

  private:
    [[property(&interesting_class::getI, &interesting_class::setI)]] int i;
  };

  void some_function();

  enum class enum_decl { first, second };

  template <typename T> struct templated_struct {
    T some_val;
  };
  
  

  class [[reflect(true)]] Transform0 {
  public:
    float getRotation() const;

  private:
    [[property(&Transform0::getRotation)]] float rotation;
  };


  class [[reflect(true)]] Transform1 {
  public:
    float getRotation() const;
    void setRotation(float val);

  private:
    [[property(&Transform1::getRotation,
               &Transform1::setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform2 {
  public:
    float getRotation() const;
    float &getRotation();
    void setRotation(float val);

  private:
    [[property(&Transform2::getRotation,
               &Transform2::setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform3 {
  public:
    float getRotation() const;
    float &getRotation();
    void setRotation(float val);
    void setRotation(float &&val);

  private:
    [[property(&Transform3::getRotation,
               &Transform3::setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform4 {
  public:
    float getRotation() const;
    float &getRotation();
    void setRotation(const float &val);
    void setRotation(float &&val);

  private:
    [[property(&Transform4::getRotation,
               &Transform4::setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform5 {
  public:
    float getRotation() const;
    float &getRotation();
    void setRotation(const float &val);
    void setRotation(float &&val);
    void setRotation(const int &val);
    void setRotation(int &&val);

  private:
    [[property(&Transform5::getRotation,
               &Transform5::setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform6 {
  public:
    static float &getRotation(Transform6 &);
    static float getRotation(const Transform6 &);
    static void setRotation(Transform6 &, const float &val);
    static void setRotation(Transform6 &, float &&val);
    static void setRotation(Transform6 &, const int &val);
    static void setRotation(Transform6 &, int &&val);

  private:
    [[property(&getRotation, &setRotation)]] float rotation;
  };

  class [[reflect(true)]] Transform7 {
  public:
    struct RotationAccessor {
      float operator()(const Transform7 &);
      float &operator()(Transform7 &);

      void operator()(Transform7 &, const float &val);
      void operator()(Transform7 &, float &&val);
      void operator()(Transform7 &, const int &val);
      void operator()(Transform7 &, int &&val);
    };

  private:
    [[property(RotationAccessor())]] float rotation;
  };

  class [[reflect(true)]] Transform8 {
  public:
    struct RotationGetter {
      float operator()(const Transform8 &);
      float &operator()(Transform8 &);
    };
    struct RotationSetter {
      void operator()(Transform8 &, const float &val);
      void operator()(Transform8 &, float &&val);
      void operator()(Transform8 &, const int &val);
      void operator()(Transform8 &, int &&val);
    };

    static constexpr auto getter = RotationGetter();
    static constexpr auto setter = RotationSetter();


  private:
    [[property(getter, setter)]] float rotation;
  };

  class [[reflect(true)]] Transform9 {
  public:
    template <typename T> T getRotation() const;

    template <typename T> void setRotation(T && val);
    
  private:
    [[property(&Transform9::getRotation<float>,
               &Transform9::setRotation)]] float rotation;
  };
  
  class [[reflect(true)]] Transform10 {
  public:
    float getRotation() const;
    float &getRotation();
    void setRotation(const float &val);
    void setRotation(float &&val);
    void setRotation(const int &val);
    void setRotation(int &&val);

  private:
    [[property(cast_fn<float(const Transform10 &)>(&Transform10::getRotation),
               (void (Transform10::*)(const float &)) &
                   Transform10::setRotation)]] float rotation;
  };
}


