/*!
 * \file      generateoperatorcall.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once
#include "definitions.hpp"

#include <bitset>

std::string to_string(GhostFunctionCallExpr::Type type);

std::string getFullyQualified(const Expr *expr, const ASTContext &Context);

std::string getFullyQualified(QualType qtype, const ASTContext &Context);

enum BigFive : unsigned {
  default_constructible,
  copy_constructible,
  move_constructible,
  copy_assignable,
  move_assignable
};

using BigFiveSet = std::bitset<5>;

std::unordered_map<AccessorType, Accessor>
generateAccessors(const Expr *expr, const FieldDecl *field,
                  const ASTContext &Context, const std::string &prefix,
                  const std::string &postfix);

GhostFunctionCallExpr generateOperatorCall(const FunctionDecl *decl,
                                           SourceLocation loc,
                                           const ASTContext &Context,
                                           const std::string &prefix,
                                           const std::string &postfix);

BigFiveSet getBigFive(QualType type);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const FieldDecl *field, [[maybe_unused]] const Expr *expr,
                     [[maybe_unused]] const ASTContext &Context,
                     const std::string &prefix, const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXConversionDecl *conversion,
                     [[maybe_unused]] const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXConstructorDecl *ctor,
                     [[maybe_unused]] const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const FunctionDecl *fn, const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXMethodDecl *method, const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const FunctionDecl *fn, const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const Expr *expr, const ASTContext &Context,
                     const std::string &prefix, const std::string &postfix);
