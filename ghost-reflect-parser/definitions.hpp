/*!
 * \author Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \file definitions.hpp
 * \date 09.30.2019
 * \copyright This file is part of Ghost-Reflect Project
 *             License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 *
 * \brief Defines common things
 */
#pragma once

#include "clang/AST/Attr.h"
#include "clang/AST/Decl.h"
#include "clang/AST/QualTypeNames.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/Dynamic/VariantValue.h"
#include "clang/Lex/Lexer.h"

#include <rapidjson/document.h>
#ifndef WIN32
#include <boost/filesystem.hpp>
#else
#include <filesystem>
#endif
#include <boost/range.hpp>

#include <iostream>

using namespace clang;
using namespace clang::ast_matchers;
using namespace clang::TypeName;

#ifndef WIN32
namespace fs = boost::filesystem;
#else
namespace fs = std::filesystem;
#endif

struct GhostFunctionCallExpr {
  enum Type : unsigned {
    method,
    function,
    object,
    op_call,
    conversion,
    constructor
  } type;

  std::vector<QualType> arg_types;
  QualType return_type;
  QualType function_type;
  std::string expr;
};

enum class AccessorType : unsigned { getter, setter };

std::string to_string(AccessorType type);

struct Accessor {
  AccessorType type;
  std::string call_expr;
  std::string original_expr;
  std::vector<std::string> signatures;
};
