#include "reflect_callback.hpp"
#include "visitor.hpp"

rapidjson::Pointer PropagateParents(
    ASTContext &Context, NamedDecl *decl, rapidjson::Document &doc,
    std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls) {

  auto parent = decl->getDeclContext();
  std::stack<NamedDecl *> parents_stack;
  rapidjson::Pointer last_pointer;

  // Fill stack of parents
  while (parent) {

    // Skip for not named parents (externs and other stuff)
    while (!isa<NamedDecl>(parent) and !isa<TranslationUnitDecl>(parent)) {
      parent = parent->getParent();
    }
    if (isa<TranslationUnitDecl>(parent))
      break;
    NamedDecl *named_parent = nullptr;

    if (auto record_parent = dyn_cast_or_null<RecordDecl>(parent))
      named_parent = record_parent->getDefinition();
    else if (auto ns_parent = dyn_cast_or_null<NamespaceDecl>(parent))
      named_parent = ns_parent->getFirstDecl();

    // Safety assert for function scopes
    assert(named_parent);
    auto iter = visited_decls.find(named_parent);
    if (iter != visited_decls.end()) {
      last_pointer = iter->second;
      break;
    } else {
      parents_stack.push(named_parent);
      parent = parent->getParent();
    }
  }

  while (!parents_stack.empty()) {
    auto top = parents_stack.top();

    if (auto record = dyn_cast_or_null<RecordDecl>(top)) {
      last_pointer =
          LightReflect(record, Context, doc, visited_decls, last_pointer);
      visited_decls.try_emplace(record, last_pointer);
    } else if (auto ns = dyn_cast_or_null<NamespaceDecl>(top)) {
      last_pointer =
          LightReflect(ns, Context, doc, visited_decls, last_pointer);
      visited_decls.try_emplace(ns, last_pointer);
    }

    parents_stack.pop();
  }

  return last_pointer;
}
