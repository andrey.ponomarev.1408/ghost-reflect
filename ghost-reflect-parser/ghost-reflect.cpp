/*!
 * \file      ghost-reflect.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "definitions.hpp"
#include "helper_decl_finder.hpp"
#include "visitor.hpp"

// Declares clang::SyntaxOnlyAction.
#include "clang/Driver/Options.h"
#include "clang/Frontend/FrontendActions.h"

#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
// Declares llvm::cl::extrahelp
#include "llvm/Support/CommandLine.h"

//#include <regex>

#include <rapidjson/prettywriter.h>

#include <fstream>

using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;

// Apply a custom category to all command-line options so that they are the
// only ones displayed.
static llvm::cl::OptionCategory GhostReflectCategory("my-tool options");
static std::unique_ptr<opt::OptTable> Options(createDriverOptTable());

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...\n");

static cl::opt<std::string> OutputFilename("o",
                                           cl::desc("Specify output filename"),
                                           cl::value_desc("filename"),
                                           cl::cat(GhostReflectCategory));
static cl::opt<std::string> VariablesPrefix(
    "variables_prefix",
    cl::desc("Specify prefix for variables for getter/setters, etc.\n"
             "For example for prefix \"$\" and struct: struct foo{int bar};\n"
             "Generated getter.call_expr would be: $_0.bar"),
    cl::value_desc("string"), cl::cat(GhostReflectCategory));
static cl::opt<std::string> VariablesPostfix(
    "variables_postfix",
    cl::desc("Specify postfix for variables for getter/setters, etc.\n"
             "For example for postfix \"$\" and struct: struct foo{int bar};\n"
             "Generated getter.call_expr would be: _0$.bar"),
    cl::value_desc("string"), cl::cat(GhostReflectCategory));

int main(int argc, const char **argv) {

  WorkingDir = fs::current_path().string();

  CommonOptionsParser OptionsParser(argc, argv, GhostReflectCategory);

  if (!OutputFilename.empty() and
      !std::ofstream(OutputFilename.c_str()).is_open()) {
    std::cerr << "Incorrect output filename '" << OutputFilename << '\''
              << std::endl;
    return 1;
  }

  // Main tool is used to work on provided sources
  ClangTool MainTool(OptionsParser.getCompilations(),
                     OptionsParser.getSourcePathList());

  int result;

  // Enable GHOST_REFLECTION macro in tools (allows attrs macro to be
  // __attributes__(annotate(...))
  MainTool.appendArgumentsAdjuster(getInsertArgumentAdjuster(
      "-DGHOST_REFLECT", ArgumentInsertPosition::BEGIN));

  HelperCallback helper_decl_engine;
  helper_cb = &helper_decl_engine;
  MatchFinder Finder;
  Finder.addMatcher(HelperDeclMatcher, &helper_decl_engine);

  result = MainTool.run(newFrontendActionFactory(&Finder).get());

  if (result != 0)
    return result;

  Prefix = VariablesPrefix.getValue();
  Postfix = VariablesPostfix.getValue();
  OutputFile = OutputFilename.getValue();

  // Parse file and fill all attributes
  result = MainTool.run(newFrontendActionFactory<GhostReflectAction>().get());
  //  if (result != 0)
  //    return result;

  //  rapidjson::StringBuffer buffer;
  //  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  //  reflect.GetDocument().Accept(writer);
  //  if (OutputFilename.empty())
  //    std::cout << buffer.GetString() << std::endl;
  //  else {
  //    std::ofstream(OutputFilename.c_str()) << buffer.GetString() <<
  //    std::flush;
  //  }

  return result;
}
