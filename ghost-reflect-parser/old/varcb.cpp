/*!
 * \file      varcb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "varcb.hpp"
#include "reflection_engine.hpp"
#include "generatefunctioncall.hpp"

#include <clang/AST/RecordLayout.h>

[[nodiscard]] rapidjson::Value Serialize(const VarDecl *var,
                                         ReflectionEngine &engine) {
  auto &alloc = engine.GetDocAllocator();

  rapidjson::Value val;
  handleCommon(var, val, engine);

  auto type_name = getFullyQualified(var->getType(), var->getASTContext());
  rapidjson::Value type(type_name.c_str(), unsigned(type_name.size()), alloc);
  rapidjson::Value getter;
  rapidjson::Value setter;

  val.AddMember("type", type, alloc);

  return val;
}

[[nodiscard]] rapidjson::Value
Serialize(const FieldDecl *field, ReflectionEngine &engine) {

  using namespace rapidjson;
  auto &alloc = engine.GetDocAllocator();

  auto &Context = field->getASTContext();
  clang::DiagnosticsEngine &DE = Context.getDiagnostics();
  const unsigned invalid_accessor =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid accessor. Should be a public "
                         "member function or function/functor "
                         "with signature return_type(object_reference) or "
                         "return_type(object_reference, value_to_set)");
  const unsigned already_defined = DE.getCustomDiagID(
      clang::DiagnosticsEngine::Error,
      "%select{Getter|Setter}0 was already defined by previous accessor.");
  const unsigned multiple_possible_accessors =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Warning,
                         "Field '%0' is public, but it has property attribute. "
                         "It should be private or protected.");
  
  
  Value val;
  handleCommon(field, val, engine);

  auto type_name = getFullyQualified(field->getType(), field->getASTContext());
  Value type(type_name.c_str(), unsigned(type_name.size()), alloc);
  Value getter(kNullType);
  Value setter(kNullType);
  
  if (field->getAccess() == AS_public and field->hasAttr<GhostPropertyAttr>()) {
    DE.Report(field->getLocation(), multiple_possible_accessors)
        << field->getNameAsString();
  }
  
  if (auto property = field->getAttr<GhostPropertyAttr>()) {

    std::unordered_map<AccessorType, Accessor> generated_acc;

    for (const auto &acc : property->accessors()) {

      auto tmp = generateAccessors(acc, field, Context);

      if (tmp.empty()) {
        DE.Report(acc->getExprLoc(), invalid_accessor);
        return Value();
      }

      for (auto &&val :
           boost::make_iterator_range(std::make_move_iterator(tmp.begin()),
                                      std::make_move_iterator(tmp.end()))) {
        auto result = generated_acc.emplace(val);
        if (!result.second) {
          DE.Report(acc->getExprLoc(), already_defined) << unsigned(val.first);
          return Value();
        }
      }
    }

    auto set_accessor = [](Value &val, const Accessor &acc,
                           Document::AllocatorType &alloc) {
      if (!val.IsObject())
        val.SetObject();
      val.AddMember("call_expr", Value(acc.call_expr.c_str(), alloc), alloc);
      val.AddMember("original_expr", Value(acc.original_expr.c_str(), alloc),
                    alloc);
      Value signatures(kArrayType);
      for (auto &sig : acc.signatures) {
        signatures.PushBack(Value(sig.c_str(), alloc), alloc);
      }
      val.AddMember("signatures", signatures, alloc);
    };

    for (auto &acc : generated_acc) {
      if (acc.first == AccessorType::getter)
        set_accessor(getter, acc.second, alloc);
      else
        set_accessor(setter, acc.second, alloc);
    }
  } else if (field->getAccess() == AS_public) {

    auto field_name = field->getNameAsString();

    auto record_type = QualType(field->getParent()->getTypeForDecl(), 0);
    auto crecord_type = record_type;
    crecord_type.addConst();
    auto record_lref = Context.getLValueReferenceType(record_type);
    auto record_rref = Context.getRValueReferenceType(record_type);
    auto record_clref = Context.getLValueReferenceType(record_type);

    auto record_lref_str = getFullyQualified(record_lref, Context);
    auto record_rref_str = getFullyQualified(record_rref, Context);
    auto record_clref_str = getFullyQualified(record_clref, Context);

    auto field_type = field->getType();
    if (field_type->isReferenceType())
      field_type = field_type->getPointeeType();
    auto cfield_type = field_type;
    cfield_type.addConst();

    auto field_lref = Context.getLValueReferenceType(field_type);
    auto field_rref = Context.getRValueReferenceType(field_type);
    auto field_clref = Context.getLValueReferenceType(cfield_type);

    auto field_lref_str = getFullyQualified(field_lref, Context);
    auto field_rref_str = getFullyQualified(field_rref, Context);
    auto field_clref_str = getFullyQualified(field_clref, Context);

    auto member_ptr_signature = "&" + getFullyQualified(record_type, Context) +
                                "::" + field->getNameAsString();

    getter.SetObject();
    getter.AddMember("call_expr", Value(("_0." + field_name).c_str(), alloc),
                     alloc);
    getter.AddMember("original_expr",
                     Value(member_ptr_signature.c_str(), alloc), alloc);
    Value getter_signatures(kArrayType);
    getter_signatures.PushBack(
        Value((field_clref_str + '(' + record_clref_str + ')').c_str(), alloc),
        alloc);
    getter_signatures.PushBack(
        Value((field_lref_str + '(' + record_lref_str + ')').c_str(), alloc),
        alloc);
    getter.AddMember("signatures", getter_signatures, alloc);

    // For setter we need to determine if it is assignable at all
    auto big_five = getBigFive(field_type);
    if (big_five.test(BigFive::copy_assignable) or
        big_five.test(BigFive::move_assignable)) {

      setter.SetObject();
      setter.AddMember("call_expr",
                       Value(("_0." + field_name + " = _1").c_str(), alloc),
                       alloc);
      setter.AddMember("original_expr",
                       Value(member_ptr_signature.c_str(), alloc), alloc);
      Value setter_signature(kArrayType);

      if (big_five.test(BigFive::copy_assignable))
        setter_signature.PushBack(
            Value((field_lref_str + '(' + record_lref_str + ", " +
                   field_clref_str + ')')
                      .c_str(),
                  alloc),
            alloc);

      if (big_five.test(BigFive::move_assignable))
        setter_signature.PushBack(
            Value((field_lref_str + '(' + record_lref_str + ", " +
                   field_rref_str + ')')
                      .c_str(),
                  alloc),
            alloc);

      setter.AddMember("signatures", setter_signature, alloc);
    }
  } else {
    return Value();
  }

  val.AddMember("type", type, alloc);
  val.AddMember("getter", getter, alloc);
  val.AddMember("setter", setter, alloc);

  return val;
}

void VarReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<VarDecl>("id");
  Assert(decl != nullptr, "Failed to get VarDecl in VarReflect");
  decl = decl->getFirstDecl();
  
  if (!decl or engine_.isRegistered(decl))
    return;

  auto val = Serialize(decl, engine_);  
  engine_.AddDecl(decl, val);
}

void ParmVarReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<VarDecl>("id");
  Assert(decl != nullptr, "Failed to get VarDecl in VarReflect");
  decl = decl->getFirstDecl();
  
  if (engine_.isRegistered(decl))
    return;
  
  auto val = Serialize(decl, engine_);    
  
  
  engine_.AddDecl(decl, val);
}

void FieldReflect::run(const MatchFinder::MatchResult &result)
{
  auto decl = result.Nodes.getNodeAs<FieldDecl>("id");
  Assert(decl != nullptr, "Failed to get FieldDecl in VarReflect");
    
//  VarDeclInfo info(decl);
  
  
  auto val = Serialize(decl, engine_);    
  
  
  
  engine_.AddDecl(decl, val);
  
}
