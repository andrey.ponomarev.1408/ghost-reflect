/*!
 * \file      enumcb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "enumcb.hpp"
#include "reflection_engine.hpp"
#include "generatefunctioncall.hpp"

void EnumReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<EnumDecl>("id");
  Assert(decl != nullptr, "Failed to get EnumDecl in EnumReflect");
  decl = dyn_cast_or_null<EnumDecl>(decl->getDefinition());


  if (!decl or engine_.isRegistered(decl))
    return;

  auto &Context = decl->getASTContext();
  auto &alloc = engine_.GetDocAllocator();

  using namespace rapidjson;

  Value val;
  handleCommon(decl, val, engine_);

  Value enumerators(kArrayType);

  for (auto enumerator : decl->enumerators()) {
    Value current_val(kObjectType);
    current_val.AddMember(
        "name", Value(enumerator->getNameAsString().c_str(), alloc), alloc);
    current_val.AddMember("value", enumerator->getInitVal().getExtValue(),
                          alloc);
    enumerators.PushBack(current_val, alloc);
  }

  val.AddMember(
      "underlying_type",
      Value(getFullyQualified(decl->getIntegerType(), Context).c_str(), alloc),
      alloc);
  val.AddMember("enumerators", enumerators, alloc);

  engine_.AddDecl(decl, val);
}
