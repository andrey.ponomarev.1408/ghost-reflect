/*!
 * \file      recordcb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "recordcb.hpp"
#include "generatefunctioncall.hpp"
#include "reflection_engine.hpp"

#include <clang/AST/CXXInheritance.h>
#include <clang/AST/RecordLayout.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/raw_os_ostream.h>

#include <bitset>
#include <queue>

void getUniqueBases(std::unordered_set<const CXXRecordDecl *> &visited_bases,
                    std::vector<const CXXRecordDecl *> &out,
                    const CXXRecordDecl *Record) {
  if (!visited_bases.count(Record)) {
    for (const auto &base : Record->bases()) {
      const RecordType *Ty = base.getType()->getAs<RecordType>();
      if (!Ty) {
        continue;
      }

      CXXRecordDecl *Base =
          cast_or_null<CXXRecordDecl>(Ty->getDecl()->getDefinition());
      if (!Base)
        continue;

      if (!visited_bases.count(Base)) {
        getUniqueBases(visited_bases, out, Base);
      }
    }

    visited_bases.insert(Record);
    out.push_back(Record);
  }
}

std::vector<const CXXRecordDecl *> getUniqueBases(const CXXRecordDecl *Record);

void RecordReflect::run(const MatchFinder::MatchResult &result) {

  auto decl = result.Nodes.getNodeAs<RecordDecl>("id");
  auto cxx = dyn_cast<CXXRecordDecl>(decl);

  Assert(decl != nullptr, "Failed to get RecordDecl in RecordReflect");
  decl = dyn_cast_or_null<CXXRecordDecl>(decl->getDefinition());
  if (!decl or engine_.isRegistered(decl))
    return;

  auto &Context = decl->getASTContext();

  rapidjson::Value val;
  auto &alloc = engine_.GetDocAllocator();

  handleCommon(decl, val, engine_);

  rapidjson::Value bases(rapidjson::kArrayType);
  rapidjson::Value all_bases(rapidjson::kArrayType);
  rapidjson::Value constructors(rapidjson::kArrayType);
  rapidjson::Value conversion_operators(rapidjson::kArrayType);
  rapidjson::Value methods(rapidjson::kArrayType);
  rapidjson::Value fields(rapidjson::kArrayType);
  rapidjson::Value variables(rapidjson::kArrayType);

  if (cxx) {
    // all bases
    auto all_bases_vec = getUniqueBases(cxx);
    for (auto base : all_bases_vec) {
      all_bases.PushBack(
          rapidjson::Value(
              getFullyQualified(QualType(base->getTypeForDecl(), 0),
                                cxx->getASTContext())
                  .c_str(),
              alloc),
          alloc);
    }

    // only direct bases
    for (auto base : cxx->bases()) {
      rapidjson::Value base_val(rapidjson::kObjectType);

      base_val.AddMember(
          "type",
          rapidjson::Value(getFullyQualified(base.getType(), Context).c_str(),
                           alloc),
          alloc);

      base_val.AddMember("is_virtual", base.isVirtual(), alloc);
      switch (base.getAccessSpecifier()) {
      case AS_public:
        base_val.AddMember("access_level", "public", alloc);
        break;
      case AS_private:
        base_val.AddMember("access_level", "private", alloc);
        break;
      case AS_protected:
        base_val.AddMember("access_level", "protected", alloc);
        break;
      case AS_none:
        base_val.AddMember("access_level", "none", alloc);
        break;
      }

      bases.PushBack(base_val, alloc);
    }

    for (auto method : cxx->methods()) {
      if (!isa<CXXDestructorDecl>(method)) {
        auto val = Serialize(method, engine_);

        if (val.IsNull())
          continue;

        if (isa<CXXConstructorDecl>(method))
          constructors.PushBack(val, alloc);
        else if (isa<CXXConversionDecl>(method))
          conversion_operators.PushBack(val, alloc);
        else
          methods.PushBack(val, alloc);
      }
    }
  }

  for (auto field : decl->fields()) {
    auto val = Serialize(field, engine_);
    if (!val.IsNull())
      fields.PushBack(val, alloc);
  }

  val.AddMember("bases", bases, alloc);
  val.AddMember("all_bases", all_bases, alloc);
  val.AddMember("is_union", decl->isUnion(), alloc);
  val.AddMember("is_abstract", (cxx ? cxx->isAbstract() : false), alloc);
  val.AddMember("has_default_ctor", !cxx ? true : cxx->hasDefaultConstructor(),
                alloc);
  val.AddMember("has_copy_ctor",
                !cxx ? true : cxx->hasCopyConstructorWithConstParam(), alloc);
  val.AddMember("has_move_ctor", cxx ? true : cxx->hasMoveConstructor(), alloc);
  val.AddMember("has_copy_assignment",
                !cxx ? true : cxx->hasCopyAssignmentWithConstParam(), alloc);
  val.AddMember("has_move_assignment", !cxx ? true : cxx->hasMoveAssignment(),
                alloc);

  val.AddMember("constructors", constructors, alloc);
  val.AddMember("conversion_operators", conversion_operators, alloc);
  val.AddMember("methods", methods, alloc);
  val.AddMember("fields", fields, alloc);
  val.AddMember("variables", variables, alloc);

  engine_.AddDecl(decl, val);
}

void RecordDescendantAttributesReflect::run(
    const MatchFinder::MatchResult &result) {

  auto decl = result.Nodes.getNodeAs<RecordDecl>("id");

  Assert(decl != nullptr,
         "Failed to get RecordDecl in RecordDescendantAttributesReflect");
  decl = dyn_cast<RecordDecl>(decl->getFirstDecl());

  //  if (!engine_.HasDeclaration(decl->getQualifiedNameAsString()))
  //    engine_.AddDeclaration(RecordDeclInfo(decl));
}

void PrintNodeCallback::run(const MatchFinder::MatchResult &result) {
  auto named_decl = result.Nodes.getNodeAs<NamedDecl>("id");
  std::cout << named_decl->getQualifiedNameAsString() << " ";
  llvm::raw_os_ostream llvm_out(std::cout);
  std::cout << "[[";
  for (const auto &attr : named_decl->getAttrs()) {
    attr->printPretty(llvm_out, PrintingPolicy(result.Context->getLangOpts()));
  }

  llvm_out.flush();

  std::cout << "]]" << std::endl;
}

void PrintReflectedNodeCallback::run(const MatchFinder::MatchResult &result) {
  auto named_decl = result.Nodes.getNodeAs<NamedDecl>("id");
  llvm::raw_os_ostream stream(std::cout);
  llvm::formatted_raw_ostream out(stream);

  out << named_decl->getQualifiedNameAsString();

  out.PadToColumn(30);

  out << "|" << named_decl->getDeclKindName() << "|\n";
}

std::string comma_separated_str(const std::vector<std::string> &vec) {
  std::stringstream out;

  auto current = vec.begin();
  while (current != vec.end()) {
    out << *current;
    ++current;
    if (current != vec.end())
      out << ", ";
  }

  return out.str();
}

void PrintPropertyNodeCallback::run(const MatchFinder::MatchResult &result) {
  PrintingPolicy print_policy(result.Context->getLangOpts());
  print_policy.FullyQualifiedName = 1;
  print_policy.SuppressScope = 0;

  result.Context->setPrintingPolicy(print_policy);
  auto var = result.Nodes.getNodeAs<FieldDecl>("id");
  auto property = var->getAttr<GhostPropertyAttr>();

  clang::DiagnosticsEngine &DE = result.Context->getDiagnostics();
  const unsigned invalid_accessor =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid accessor. Should be a public "
                         "member function or function/functor "
                         "with signature return_type(object_reference) or "
                         "return_type(object_reference, value_to_set)");
  const unsigned already_defined = DE.getCustomDiagID(
      clang::DiagnosticsEngine::Error,
      "%select{Getter|Setter}0 was already defined by previous accessor.");

  llvm::raw_os_ostream raw_os(std::cout);
  llvm::formatted_raw_ostream os(raw_os);

  static bool printed_columns = false;

  if (!printed_columns) {
    os << "Field Name";
    os.PadToColumn(35);
    os << "Expr Type";
    os.PadToColumn(50);
    os << "Expr";
    os.PadToColumn(85);
    os << "Signatures";
    os << "\n";
    printed_columns = true;
  }

  std::unordered_map<AccessorType, Accessor> generated_acc;

  for (const auto &acc : property->accessors()) {

    auto tmp = generateAccessors(acc, var, *result.Context);

    if (tmp.empty()) {
      DE.Report(acc->getExprLoc(), invalid_accessor);
      return;
    }

    for (auto &&val :
         boost::make_iterator_range(std::make_move_iterator(tmp.begin()),
                                    std::make_move_iterator(tmp.end()))) {
      auto result = generated_acc.emplace(val);
      if (!result.second) {
        DE.Report(acc->getExprLoc(), already_defined) << unsigned(val.first);
        return;
      }
    }
  }

  for (auto &accessor : generated_acc) {

    os << var->getQualifiedNameAsString();
    // acc type
    os.PadToColumn(35);
    os << to_string(accessor.second.type);
    // acc call expr
    os.PadToColumn(50);
    os << accessor.second.call_expr;
    // acc signatures
    os.PadToColumn(85);
    for (auto &sig : accessor.second.signatures)
      os << sig << " | ";
    os << "\n";
    os.flush();
  }
  std::cout << std::endl;
}
