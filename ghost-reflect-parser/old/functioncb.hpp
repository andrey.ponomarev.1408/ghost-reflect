/*!
 * \file      functioncb.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"
#include "recordcb.hpp"

static DeclarationMatcher FunctionMatcher =
    functionDecl(isReflected, unless( cxxMethodDecl() )).bind("id");

static DeclarationMatcher MethodMatcher =
    cxxMethodDecl(isReflected, unless(cxxDestructorDecl())).bind("id");

class FunctionReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
private:
  void run(const MatchFinder::MatchResult &result) override;
};

[[nodiscard]] rapidjson::Value
Serialize(const CXXMethodDecl *decl, ReflectionEngine &engine);

