/*!
 * \file      reflection_engine.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"
#include "enumcb.hpp"
#include "functioncb.hpp"
#include "generatefunctioncall.hpp"
#include "namespacecb.hpp"
#include "recordcb.hpp"
#include "varcb.hpp"

#include "clang/ASTMatchers/Dynamic/Parser.h"

#include <rapidjson/allocators.h>
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <rapidjson/prettywriter.h>

using namespace boost;

class InvalidReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;

private:
  enum class refl_t : uint {
    ak_class = 0,
    ak_struct,
    ak_union,
    ak_enum,
    ak_variable,
    ak_function,
  } reflected_type;
  enum class scope_t : uint {
    ak_funcion = 0,
    ak_namespace,
    ak_private
  } scope_type;

  void run(const MatchFinder::MatchResult &result) override;
};

class ReflectionEngine {
public:
  InvalidReflect invalid_cb{*this};

  TranslationUnitReflect tr_unit_cb{*this};
  RecordReflect record_cb{*this};
  FunctionReflect fn_cb{*this};
  VarReflect var_cb{*this};
  EnumReflect enum_cb{*this};

  MatchFinder finder; // Finder of reflection data

  ReflectionEngine();

  std::pair<const Decl *, rapidjson::Pointer> getParent(const Decl *d) const;

  rapidjson::Document::AllocatorType &GetDocAllocator();

  const rapidjson::Document &GetDocument() const { return doc_; }

  bool isRegistered(const Decl *decl);

  std::optional<rapidjson::Pointer> getPointerForDecl(const Decl *d) const;

  void AddDecl(const Decl *d, rapidjson::Value &val);

  void DeclareHelper(const GhostDeclareHelperAttr *action_decl,
                     const ASTContext &Context);

  rapidjson::Value getHelperValue(const GhostHelperAttr *action,
                                  const ASTContext &Context);

private:
  static const DeclContext *getSimpleParent(const Decl *d);

  std::unordered_map<const Decl *, rapidjson::Pointer> registered_decls_;
  std::unordered_multimap<std::string, const GhostDeclareHelperAttr *>
      declared_actions_;

  rapidjson::Document doc_;
};
