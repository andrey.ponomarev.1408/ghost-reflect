/*!
 * \file      varcb.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"

static DeclarationMatcher VarMatcher =
    varDecl(isReflected, unless(anyOf(parmVarDecl(), hasLocalStorage(), isStaticLocal()))).bind("id");

static DeclarationMatcher ParameterMatcher =
    varDecl(isReflected, hasParent(functionDecl(isReflected))).bind("id");

static DeclarationMatcher FieldMatcher =
    fieldDecl(anyOf(isReflected, hasAttr(attr::GhostProperty))).bind("id");



[[nodiscard]] rapidjson::Value
Serialize(const FieldDecl *field, ReflectionEngine &engine);
[[nodiscard]] rapidjson::Value
Serialize(const VarDecl *var, ReflectionEngine &engine);

class VarReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;

private:
  void run(const MatchFinder::MatchResult &result) override;
};

class ParmVarReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
  
private:
  void run(const MatchFinder::MatchResult &result) override;
};


class FieldReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
  
private:
  void run(const MatchFinder::MatchResult &result) override;
};
