/*!
 * \file      namespacecb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "namespacecb.hpp"
#include "reflection_engine.hpp"

void NamespaceDescendantAttributesReflect::run(
    const clang::ast_matchers::MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<NamespaceDecl>("id");
  Assert(decl != nullptr,
         "Failed to get NamespaceDecl in NamespaceDescendantAttributesReflect");
  decl = decl->getFirstDecl();
  
//  if (!engine_.HasDeclaration(decl->getQualifiedNameAsString()))
//    engine_.AddDeclaration(NamespaceDeclInfo(DeclInfo(decl)));
}

void NamespaceReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<NamespaceDecl>("id");
  Assert(decl != nullptr, "Failed to get NamespaceDecl in NamespaceReflect");

  rapidjson::Value val;

  handleCommon(decl, val, engine_);
  

  rapidjson::Value namespaces(rapidjson::kArrayType);
  rapidjson::Value records(rapidjson::kArrayType);
  rapidjson::Value enums(rapidjson::kArrayType);
  rapidjson::Value functions(rapidjson::kArrayType);
  rapidjson::Value variables(rapidjson::kArrayType);
  

  val.AddMember("namespaces", namespaces, engine_.GetDocAllocator());
  val.AddMember("records", records, engine_.GetDocAllocator());
  val.AddMember("enums", enums, engine_.GetDocAllocator());
  val.AddMember("functions", functions, engine_.GetDocAllocator());
  val.AddMember("variables", variables, engine_.GetDocAllocator());
  

  engine_.AddDecl(decl, val);

  
//  if (!engine_.HasDeclaration(decl->getQualifiedNameAsString()))
//    engine_.AddDeclaration(NamespaceDeclInfo(DeclInfo(decl)));
}


void TranslationUnitReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<TranslationUnitDecl>("id");
  Assert(decl != nullptr, "Failed to get NamespaceDecl in NamespaceReflect");
  
  rapidjson::Value val(rapidjson::kObjectType);
  
  rapidjson::Value namespaces(rapidjson::kArrayType);
  rapidjson::Value records(rapidjson::kArrayType);
  rapidjson::Value enums(rapidjson::kArrayType);
  rapidjson::Value functions(rapidjson::kArrayType);
  
  val.AddMember("namespaces", namespaces, engine_.GetDocAllocator());
  val.AddMember("records", records, engine_.GetDocAllocator());
  val.AddMember("enums", enums, engine_.GetDocAllocator());
  val.AddMember("functions", functions, engine_.GetDocAllocator());
  
  engine_.AddDecl(decl, val);
  
  //  if (!engine_.HasDeclaration(decl->getQualifiedNameAsString()))
  //    engine_.AddDeclaration(NamespaceDeclInfo(DeclInfo(decl)));
}
