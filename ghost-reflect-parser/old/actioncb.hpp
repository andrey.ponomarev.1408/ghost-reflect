/*!
 * \file      actioncb.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"

static DeclarationMatcher ActionDeclarationMatcher =
    decl(hasAttr(attr::GhostDeclareHelper)).bind("id");

class ActionDeclarationReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;

private:
  void run(const MatchFinder::MatchResult &result) override;
};
