/*!
 * \file      reflection_engine.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "reflection_engine.hpp"
#include "definitions.hpp"

#include <boost/tokenizer.hpp>

class StringSeparator {
public:
  StringSeparator(const std::string &str) : str_(str) {}

  StringSeparator(std::string &&str) : str_(str) {}

  template <typename Iter, typename Tok>
  bool operator()(Iter &next, const Iter &end, Tok &tok) const {
    if (next == end) {
      return false;
    }
    auto foundToken = std::search(next, end, str_.begin(), str_.end());
    tok.assign(next, foundToken);
    next = (foundToken == end) ? end : foundToken + str_.size();
    return true;
  }

  void reset() {}

private:
  std::string str_;
};

void InvalidReflect::run(
    const clang::ast_matchers::MatchFinder::MatchResult &result) {

  using ArgumentKind = DiagnosticsEngine::ArgumentKind;

  const ASTContext &Context = *result.Context;
  clang::DiagnosticsEngine &DE = Context.getDiagnostics();
  const unsigned ID = DE.getCustomDiagID(
      clang::DiagnosticsEngine::Error,
      "Reflected %select{class|struct|union|enum|variable|function}0 "
      "%q1 is defined inside %select{function|anonymous namespace|private "
      "scope}2.");

  auto decl = result.Nodes.getNodeAs<NamedDecl>("id");
  auto record = result.Nodes.getNodeAs<RecordDecl>("id");
  auto variable = result.Nodes.getNodeAs<VarDecl>("id");
  auto fn = result.Nodes.getNodeAs<FunctionDecl>("id");

  Assert(record or variable or fn,
         "Declaration is neither record nor variable nor fn");

  auto scope_fn = result.Nodes.getNodeAs<FunctionDecl>("parent");
  auto scope_namespace = result.Nodes.getNodeAs<NamespaceDecl>("parent");

  if (record) {
    using tag = RecordDecl::TagKind;

    switch (record->getTagKind()) {
    case tag::TTK_Enum:
      reflected_type = refl_t::ak_enum;
      break;
    case tag::TTK_Class:
      reflected_type = refl_t::ak_class;
      break;
    case tag::TTK_Union:
      reflected_type = refl_t::ak_union;
      break;
    case tag::TTK_Struct:
      reflected_type = refl_t::ak_struct;
      break;
    default:
      Assert(0, "I have no idea what TTK_Interface is");
      break;
    }
  } else if (variable) {
    reflected_type = refl_t::ak_variable;
  } else if (fn) {
    reflected_type = refl_t::ak_function;
  }

  if (scope_fn)
    scope_type = scope_t::ak_funcion;
  else if (scope_namespace)
    scope_type = scope_t::ak_namespace;
  else
    scope_type = scope_t::ak_private;

  auto decl_ptr = reinterpret_cast<intptr_t>(decl);
  auto DB = DE.Report(decl->getBeginLoc(), ID);
  DB.AddTaggedVal(uint(reflected_type), ArgumentKind::ak_uint);
  DB.AddTaggedVal(decl_ptr, ArgumentKind::ak_nameddecl);
  DB.AddTaggedVal(uint(scope_type), ArgumentKind::ak_uint);
}

// static DeclarationMatcher reflected_decl =
//    namedDecl(
//        anyOf(hasAttr(attr::Kind::GhostReflect),
//              hasAncestor(
//                  namedDecl(hasAttr(attr::Kind::GhostReflect)).bind("parent"))))
//        .bind("id");

static DeclarationMatcher decl_with_custom_attr =
    namedDecl(hasAttr(attr::Kind::GhostFlag)).bind("id");

static DeclarationMatcher isProperty =
    namedDecl(hasAttr(attr::Kind::GhostProperty)).bind("id");

ReflectionEngine::ReflectionEngine() {

  registered_decls_.max_load_factor(0.7f);

  finder.addMatcher(translationUnitDecl().bind("id"), &tr_unit_cb);

  finder.addMatcher(RecordMatcher, &record_cb);
  finder.addMatcher(FunctionMatcher, &fn_cb);
  finder.addMatcher(VarMatcher, &var_cb);

  finder.addMatcher(EnumMatcher, &enum_cb);
}

std::pair<const Decl *, rapidjson::Pointer>
ReflectionEngine::getParent(const Decl *d) const {
  auto simple_parent = getSimpleParent(d);

  auto out_iter = registered_decls_.find(dyn_cast<Decl>(simple_parent));
  assert(out_iter != registered_decls_.end());

  return *out_iter;
}

rapidjson::Document::AllocatorType &ReflectionEngine::GetDocAllocator() {
  return doc_.GetAllocator();
}

bool ReflectionEngine::isRegistered(const Decl *decl) {
  return registered_decls_.count(decl) >= 1;
}

std::optional<rapidjson::Pointer>
ReflectionEngine::getPointerForDecl(const Decl *d) const {
  auto iter = registered_decls_.find(d);
  if (iter != registered_decls_.end())
    return iter->second;
  else
    return std::optional<rapidjson::Pointer>();
}

void ReflectionEngine::AddDecl(const Decl *d, rapidjson::Value &val) {

  if (!isa<TranslationUnitDecl>(d)) {
    auto parent = getParent(d);
    assert(!isa<FunctionDecl>(parent.first) or isa<ParmVarDecl>(d));

    std::string push_to;

    if (isa<NamespaceDecl>(d))
      push_to = "namespaces";
    else if (isa<RecordDecl>(d))
      push_to = "records";
    else if (isa<FunctionDecl>(d))
      push_to = "functions";
    else if (isa<EnumDecl>(d))
      push_to = "enums";
    else if (isa<VarDecl>(d))
      push_to = "variables";

    assert(!push_to.empty());

    //      rapidjson::StringBuffer buf;
    //      rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buf);
    //      std::cout << "parent.val_p_: ";
    //      parent.val_p_.Stringify(buf);
    //      std::cout << buf.GetString() << std::endl;
    //      buf.Clear();

    //      doc_.Accept(writer);
    //      std::cout << buf.GetString() << std::endl;

    auto &container = (*parent.second.Get(doc_))[push_to.c_str()];

    container.PushBack(val, doc_.GetAllocator());

    rapidjson::Pointer new_pointer;
    auto last_elem = container.Size() - 1;
    new_pointer =
        parent.second.Append(push_to.c_str(), unsigned(push_to.size()))
            .Append(last_elem);

    registered_decls_.emplace(d, new_pointer);

  } else {
    if (!doc_.IsObject()) {
      rapidjson::Value namespaces(rapidjson::kArrayType);
      rapidjson::Value records(rapidjson::kArrayType);
      rapidjson::Value enums(rapidjson::kArrayType);
      rapidjson::Value functions(rapidjson::kArrayType);
      rapidjson::Value variables(rapidjson::kArrayType);

      doc_.SetObject();
      doc_.AddMember("namespaces", namespaces, doc_.GetAllocator());
      doc_.AddMember("records", records, doc_.GetAllocator());
      doc_.AddMember("enums", enums, doc_.GetAllocator());
      doc_.AddMember("functions", functions, doc_.GetAllocator());
      doc_.AddMember("variables", variables, doc_.GetAllocator());
    }
    registered_decls_.insert_or_assign(d, rapidjson::Pointer());
  }
}

void ReflectionEngine::DeclareHelper(const GhostDeclareHelperAttr *action_decl,
                                     const ASTContext &Context) {
  auto &DE = Context.getDiagnostics();
  unsigned error =
      DE.getCustomDiagID(DiagnosticsEngine::Error,
                         "Redefinition of action '%0' with %1 number of args.");

  StringRef name = action_decl->getName();
  auto same_range_pair = declared_actions_.equal_range(name.data());
  auto same_range = boost::make_iterator_range(same_range_pair);
  for (auto already_declared : same_range) {
    if (already_declared.second->helpers_size() ==
        action_decl->helpers_size()) {
      DE.Report(action_decl->getLocation(), error)
          << name << action_decl->helpers_size();
      return;
    }
  }
  declared_actions_.emplace(name.str(), action_decl);
}

rapidjson::Value ReflectionEngine::getHelperValue(const GhostHelperAttr *helper,
                                                  const ASTContext &Context) {
  auto &DE = Context.getDiagnostics();
  unsigned error =
      DE.getCustomDiagID(DiagnosticsEngine::Error,
                         "Action with %0 number of helpers wasn't declared "
                         "before. Ignoring action attribute.");

  unsigned cant_resolve_function_call = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "Action helper can't be evaluated as callable and "
      "action declaration dosn't allow non function arguments");

  using namespace rapidjson;
  Value val(kObjectType);

  clang::StringRef name = helper->getName();
  auto same_range_pair = declared_actions_.equal_range(name.data());
  auto same_range = boost::make_iterator_range(same_range_pair);

  const GhostDeclareHelperAttr *decl = nullptr;

  for (auto declared : same_range) {
    if (declared.second->helpers_size() == helper->helpers_size()) {
      decl = declared.second;
      break;
    }
  }

  if (!decl) {
    DE.Report(helper->getLocation(), error) << helper->helpers_size();
    return Value();
  }

  auto &alloc = GetDocAllocator();

  auto decl_current = decl->helpers_begin();
  auto helper_current = helper->helpers_begin();

  for (; helper_current != helper->helpers_end();
       ++helper_current, ++decl_current) {


         auto call_exprs = GetFunctionCallExprs(*action_current, Context);
       if (call_exprs.empty() and !decl->getAllowRawExprs()) {
         DE.Report((*action_current)->getExprLoc(),
         cant_resolve_function_call); return Value();
       } else if (call_exprs.empty()) {
         Value current_helper(kObjectType);

         Value call_exprs_val;
         Value original_expr(getFullyQualified(*action_current,
         Context).data(),
                             alloc);

         current_helper.AddMember("original_expr", original_expr, alloc);
         current_helper.AddMember("call_exprs", call_exprs_val, alloc);

         val.AddMember(Value(decl_current->data(), alloc), current_helper,
         alloc);
       } else {
         Value current_helper(kObjectType);
         Value call_exprs_val(kArrayType);
         Value original_expr(getFullyQualified(*action_current,
         Context).data(),
                             alloc);

         for (const auto &current_fn_call : call_exprs) {
           Value current_call_expr_val(kObjectType);
           current_call_expr_val.AddMember(
               "expr", Value(current_fn_call.expr.c_str(), alloc), alloc);
           std::string signature =
               getFullyQualified(current_fn_call.return_type, Context) + '(';
           for (unsigned i = 0; current_fn_call.arg_types.size(); ++i) {
             signature += getFullyQualified(current_fn_call.arg_types[i],
             Context); if (i + 1 != current_fn_call.arg_types.size())
               signature += ", ";
           }
           signature += ')';
           current_call_expr_val.AddMember("signature",
                                           Value(signature.c_str(), alloc),
                                           alloc);
           call_exprs_val.PushBack(current_call_expr_val, alloc);
         }

         current_helper.AddMember("original_expr", original_expr, alloc);
         current_helper.AddMember("call_exprs", call_exprs_val, alloc);

         val.AddMember(Value(decl_current->data(), alloc), current_helper,
         alloc);
       }
  }

  return val;
}

const DeclContext *ReflectionEngine::getSimpleParent(const Decl *d) {
  auto parent = d->getDeclContext();
  bool is_type_decl = isa<TagDecl>(d);
  while (parent and !isa<TranslationUnitDecl>(parent) and
         (is_type_decl or !isa<RecordDecl>(parent))) {
    parent = parent->getParent();
  }
  return parent;
}

void graph_writer(std::ostream &out);

struct occurence {
  static constexpr auto npos = std::string::npos;

  size_t count = 0;
  size_t before_last = npos;
  size_t last = npos;
};

occurence find_occurrences(const std::string &str, const std::string &sub) {
  static constexpr auto npos = std::string::npos;

  if (sub.length() == 0)
    return occurence();
  occurence occ{0, npos, npos};
  for (size_t offset = str.find(sub); offset != std::string::npos;
       offset = str.find(sub, offset + sub.length())) {
    ++occ.count;
    occ.before_last = occ.last;
    occ.last = offset;
  }
  return occ;
}

template <typename Iterator,
          typename Comp = std::equal_to<
              typename std::iterator_traits<Iterator>::value_type>>
long first_difference(Iterator lbegin, Iterator lend, Iterator rbegin,
                      Iterator rend, const Comp &comp = Comp()) {
  auto l = lbegin;
  auto r = rbegin;

  while (l != lend and r != rend and comp(*l, *r)) {
    ++l;
    ++r;
  }

  return std::distance(lbegin, l);
}

template <typename Container,
          typename Comp = std::equal_to<typename std::iterator_traits<
              decltype(std::cbegin(std::declval<const Container &>()))>>>
long first_difference(const Container &lhs, const Container &rhs,
                      const Comp &comp = Comp()) {
  return first_difference(std::cbegin(lhs), std::cend(lhs), std::cbegin(rhs),
                          std::cend(rhs), comp);
}
