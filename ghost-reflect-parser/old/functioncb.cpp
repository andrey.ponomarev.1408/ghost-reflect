/*!
 * \file      functioncb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "functioncb.hpp"
#include "reflection_engine.hpp"
#include "varcb.hpp"

void FunctionReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<FunctionDecl>("id");
  
  Assert(decl != nullptr, "Failed to get FunctionDecl in FunctionReflect");
  decl = decl->getFirstDecl();
  
  if (!decl or engine_.isRegistered(decl))
    return;
  
  auto &alloc = engine_.GetDocAllocator();
  
  rapidjson::Value val;
  handleCommon(decl, val, engine_);
  
  rapidjson::Value arguments(rapidjson::kArrayType);


  for (auto parm : decl->parameters()) {
    arguments.PushBack(Serialize(parm, engine_), alloc);
  }
  
  val.AddMember("arguments", arguments, engine_.GetDocAllocator());
  engine_.AddDecl(decl, val);
  
  
}

[[nodiscard]] rapidjson::Value
Serialize(const CXXMethodDecl *decl,
          ReflectionEngine &engine) {
  rapidjson::Value val;
  auto &alloc = engine.GetDocAllocator();
  handleCommon(decl, val, engine);

  rapidjson::Value arguments(rapidjson::kArrayType);
  
  for (auto parm : decl->parameters()) {
    arguments.PushBack(Serialize(parm, engine), alloc);
  }
  
  rapidjson::GenericStringRef<char> access("");


  val.AddMember("arguments", arguments, alloc);
  val.AddMember("is_static", decl->isStatic(), alloc);
  val.AddMember("is_virtual", decl->isVirtual(), alloc);
  val.AddMember("is_const", decl->isConst(), alloc);

  return val;
}
