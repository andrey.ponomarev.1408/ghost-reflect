#pragma once
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Lex/Lexer.h"
#include <boost/filesystem.hpp>


#include <iostream>
#include <variant>

#include "definitions.hpp"

using namespace clang;
using namespace clang::ast_matchers;









template<typename T>
void print_decl(const T* decl) {
  if (decl) {
    std::cout << decl->getName().str() << ": ";
    for (auto attr : decl->attrs()) {
      if (attr->getKind() == attr::Annotate) {
        auto annotate_attr = cast<const AnnotateAttr*>(attr);
        std::cout << annotate_attr->getAnnotation().str() << ";";
      }
    }
    std::cout << std::endl;
  }
}







class ReflectCallback: public MatchFinder::MatchCallback {
  
public:
  
  
  
  using InfoVariant = std::variant<FieldDeclInfo, FunctionDeclInfo, /*RecordDeclInfo,*/ EnumDeclInfo>;
  
private:
  virtual void
  run(const MatchFinder::MatchResult &result) override {
    
    auto& src_manager = *result.SourceManager;
    auto& context = *result.Context;
    
    const NamedDecl* decl = result.Nodes.getNodeAs<NamedDecl>("id");
    if (!decl) {
      std::cerr << "This program should proccess only named declarations, but it doesn't. There is bug somewhere in the code. " << std::endl;
      abort();
    }
    

    LocationInfo location;
    bool skip_trailing_white_spaces_and_newlines = true;
    auto src_location = decl->getEndLoc();
    src_location = Lexer::getLocForEndOfToken(src_location, 0, src_manager,
                                              context.getLangOpts());
    src_location = Lexer::findLocationAfterToken(
        src_location, tok::TokenKind::semi, src_manager, context.getLangOpts(),
        skip_trailing_white_spaces_and_newlines);
    
    location.file_ = src_manager.getFilename(src_location).str();
    
    DeclInfo decl_info = {
        decl->getName().str(),
        {},
        std::move(location)
    };
    
    for (auto att : decl->getAttrs()) {
      if (att->getKind() == attr::Kind::Annotate) {
        auto annotation = cast<const AnnotateAttr*>(att);
        decl_info.attributes_.emplace_back(annotation->getAnnotation().str());
      }
    }
    

    const TagDecl *tag_decl = result.Nodes.getNodeAs<TagDecl>("id");
    if (tag_decl)
      {
        TagDeclInfo tag_info{decl_info};

        const RecordDecl *record =
            result.Nodes.getNodeAs<clang::RecordDecl>("id");
        if (record)
          {
//          info_.emplace_back(RecordDeclInfo(DeclInfo(tag_info)));
          }
          
        const EnumDecl* enm = result.Nodes.getNodeAs<clang::EnumDecl>("id");
          if (enm) {
            info_.emplace_back(EnumDeclInfo{tag_info});
          }

      }

    const DeclaratorDecl *declarator_decl = result.Nodes.getNodeAs<DeclaratorDecl>("id");
    if (declarator_decl) {
      DeclaratorDeclInfo declarator_info{decl_info};
      
      const FieldDecl* field =
          result.Nodes.getNodeAs<clang::FieldDecl>("id");
      if (field) {
        info_.emplace_back(FieldDeclInfo{declarator_info});
      }
      
      
      const FunctionDecl *fn =
          result.Nodes.getNodeAs<clang::FunctionDecl>("id");
      if (fn)
      {
        info_.emplace_back(FunctionDeclInfo{declarator_info});
      }

    }
    

  }

public:
  std::vector<InfoVariant> info_;
};
