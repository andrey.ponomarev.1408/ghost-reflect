/*!
 * \file      actioncb.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "actioncb.hpp"

#include "reflection_engine.hpp"

void ActionDeclarationReflect::run(const MatchFinder::MatchResult &result) {
  auto decl = result.Nodes.getNodeAs<EmptyDecl>("id");
  Assert(decl != nullptr,
         "Failed to get EmptyDecl in ActionDeclarationReflect");

  for (auto attr : decl->attrs()) {
    if (auto action_decl = dyn_cast<GhostDeclareHelperAttr>(attr)) {
      engine_.DeclareHelper(action_decl, *result.Context);
    }
  }
}
