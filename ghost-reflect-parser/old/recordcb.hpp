/*!
 * \file      recordcb.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"

static DeclarationMatcher RecordMatcher =
    recordDecl(isReflected, unless(cxxRecordDecl(isLambda()))).bind("id");


//static DeclarationMatcher RecordDescendantAttributesMatcher =
//    recordDecl(hasDescendant(AttributeMatcher)).bind("id");

class RecordReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;

private:
  void run(const MatchFinder::MatchResult &result) override;

public:
};

class RecordDescendantAttributesReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
  
private:
  void run(const MatchFinder::MatchResult &result) override;
  
};

class PrintNodeCallback : public MatchFinder::MatchCallback {
private:
  void run(const MatchFinder::MatchResult &result) override;
};

class PrintReflectedNodeCallback : public MatchFinder::MatchCallback {
private:
  void run(const MatchFinder::MatchResult &result) override;
};

class PrintPropertyNodeCallback : public MatchFinder::MatchCallback {
private:
  void run(const MatchFinder::MatchResult &result) override;
};
