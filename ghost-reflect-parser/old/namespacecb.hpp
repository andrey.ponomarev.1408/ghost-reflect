/*!
 * \file      namespacecb.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "definitions.hpp"
#include "recordcb.hpp"

static DeclarationMatcher NamespaceMatcher = 
    namespaceDecl(isReflected).bind("id");

//static DeclarationMatcher NamespaceDescendantAttributesMatcher =
//    namespaceDecl(hasDescendant(AttributeMatcher)).bind("id");

class NamespaceReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
private:
  void run(const MatchFinder::MatchResult &result) override;
};

class TranslationUnitReflect : public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
private:  
  void run(const MatchFinder::MatchResult &result) override;
  
};

class NamespaceDescendantAttributesReflect: public ReflectionCallback {
public:
  using ReflectionCallback::ReflectionCallback;
private:
  void run(const MatchFinder::MatchResult &result) override;
};
