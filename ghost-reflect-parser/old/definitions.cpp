/*!
 * \file      definitions.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "definitions.hpp"
#include "generatefunctioncall.hpp"
#include "reflection_engine.hpp"

#include <boost/tokenizer.hpp>

#include <clang/Tooling/Tooling.h>
#include <llvm/Support/raw_ostream.h>

void __M_Assert(const char *expr_str, bool expr, const char *file, int line,
                const char *msg) {
  if (!expr) {
    std::cerr << "Assert failed:\t" << msg << "\n"
              << "Expected:\t" << expr_str << "\n"
              << "Source:\t\t" << file << ", line " << line << "\n";
    abort();
  }
}

void __M_Assert(const char *expr_str, bool expr, const char *file, int line,
                const std::string &msg) {
  __M_Assert(expr_str, expr, file, line, msg.c_str());
}

static PrintingPolicy getFullyQualPolicy(const ASTContext &Context) {
  PrintingPolicy policy(Context.getLangOpts());
  policy.adjustForCPlusPlus();
  policy.FullyQualifiedName = 1;
  policy.SuppressScope = 0;
  policy.PrintCanonicalTypes = 1;
  return policy;
}

static fs::path GetFileOfDecl(const NamedDecl *decl) {
  if (auto tag = dyn_cast<TagDecl>(decl)) // enum,class,union
    decl = tag->getDefinition();
  else if (auto fn = dyn_cast<FunctionDecl>(decl))
    decl = fn->getFirstDecl();
  else if (auto var = dyn_cast<VarDecl>(decl))
    decl = var->getFirstDecl();
  else if (auto field = dyn_cast<FieldDecl>(decl))
    decl = field->getFirstDecl();

  if (!decl)
    return "";
  else {
    auto &sm = decl->getASTContext().getSourceManager();
    return sm.getFilename(decl->getLocation()).str();
  }
}

DeclInfo::DeclInfo(const NamedDecl *decl)
    : file_(GetFileOfDecl(decl)), name_(decl->getQualifiedNameAsString()),
      short_name_(decl->getNameAsString()) {}

VarDeclInfo::VarDeclInfo(const VarDecl *decl)
    : DeclInfo(decl),
      type_(decl->getType()
                .getDesugaredType(decl->getASTContext())
                .getAsString(getFullyQualPolicy(decl->getASTContext()))),
      init_(decl->hasInit()
                ? getFullyQualified(decl->getInit(), decl->getASTContext())
                : "") {
  decl_type_ = DeclType::Var;
  //! \todo Generate accessors
}

VarDeclInfo::VarDeclInfo(const FieldDecl *decl)
    : DeclInfo(decl),
      type_(decl->getType()
                .getDesugaredType(decl->getASTContext())
                .getAsString(getFullyQualPolicy(decl->getASTContext()))),
      init_(decl->hasInClassInitializer()
                ? getFullyQualified(decl->getInClassInitializer(),
                                    decl->getASTContext())
                : "") {}

FieldDeclInfo::FieldDeclInfo(const FieldDecl *decl)
    : VarDeclInfo(decl), access_specifier_(decl->getAccess()) {
  decl_type_ = DeclType::Field;
}

TemplatedInfo::TemplatedInfo(const FunctionDecl *) {}
TemplatedInfo::TemplatedInfo(const RecordDecl *) {}

FunctionDeclInfo::FunctionDeclInfo(const FunctionDecl *decl)
    : DeclInfo(decl), TemplatedInfo(decl) {
  decl_type_ = DeclType::Function;
}

EnumDeclInfo::EnumDeclInfo(const EnumDecl *decl)
    : DeclInfo(decl), underlying_type_(getFullyQualified(
                          decl->getIntegerType(), decl->getASTContext())) {
  decl_type_ = DeclType::Enum;
  values_.reserve(
      size_t(std::distance(decl->enumerator_begin(), decl->enumerator_end())));
  for (auto val : decl->enumerators()) {
    values_.push_back(val->getNameAsString());
  }
}

// RecordDeclInfo::RecordDeclInfo(const RecordDecl *decl)
//    : DeclInfo(decl), TemplatedInfo(decl), is_union(decl->isUnion()) {
//  decl_type_ = DeclType::Record;
//  fields_.reserve(
//      size_t(std::distance(decl->field_begin(), decl->field_end())));
//  for (auto field : decl->fields()) {
//    fields_.emplace_back(field);
//  }

//  if (auto cxx = dyn_cast<CXXRecordDecl>(decl)) {
//    methods_.reserve(
//        size_t(std::distance(cxx->method_begin(), cxx->method_end())));
//    for (auto method : cxx->methods()) {
//      methods_.emplace_back(method);
//    }
//  }
//}

void handleCommon(const NamedDecl *named, rapidjson::Value &val,
                  ReflectionEngine &engine) {
  using namespace rapidjson;

  auto &Alloc = engine.GetDocAllocator();

  val.SetObject();

  auto &Context = named->getASTContext();
  auto &SM = Context.getSourceManager();

  Value name(kNullType);
  if (auto record = dyn_cast<RecordDecl>(named)) {
    name =
        Value((getFullyQualified(QualType(record->getTypeForDecl(), 0), Context)
                   .c_str()),
              Alloc);
  } else if (auto fn = dyn_cast<FunctionDecl>(named)) {
    auto method = dyn_cast<CXXMethodDecl>(fn);
    if (!method) {
      auto parent = fn->getParent();
      while (!isa<TranslationUnitDecl>(parent) and !isa<RecordDecl>(parent) and
             !isa<NamespaceDecl>(parent)) {
        parent = parent->getParent();
      }
      if (auto record_parent = dyn_cast<RecordDecl>(parent))
        name =
            Value((getFullyQualified(
                       QualType(record_parent->getTypeForDecl(), 0), Context) +
                   "::" + fn->getNameAsString())
                      .c_str(),
                  Alloc);
      else
        name = Value(fn->getQualifiedNameAsString().c_str(), Alloc);
    }
  } else if (auto var = dyn_cast<VarDecl>(named)) {
    if (!isa<ParmVarDecl>(var)) {
      auto parent = var->getDeclContext();
      while (!isa<TranslationUnitDecl>(parent) and !isa<RecordDecl>(parent) and
             !isa<NamespaceDecl>(parent)) {
        parent = parent->getParent();
      }
      if (auto record_parent = dyn_cast<RecordDecl>(parent))
        name =
            Value((getFullyQualified(
                       QualType(record_parent->getTypeForDecl(), 0), Context) +
                   "::" + var->getNameAsString())
                      .c_str(),
                  Alloc);
      else
        name = Value(var->getQualifiedNameAsString().c_str(), Alloc);
    }
  }

  if (name.IsNull()) {
    name = Value(named->getNameAsString().c_str(), Alloc);
  }

  Value location(kObjectType);
  Value flags(kObjectType);
  Value helpers(kObjectType);

  auto file_id = SM.getFileID(named->getLocation());
  auto file_offset = SM.getFileOffset(named->getLocation());
  auto col_number = SM.getColumnNumber(file_id, file_offset);
  auto row_number = SM.getLineNumber(file_id, file_offset);

  location.AddMember(
      "file",
      Value(fs::relative(SM.getFilename(named->getLocation()).data()).c_str(),
            Alloc),
      Alloc);
  location.AddMember("file_offset", file_offset, Alloc);
  location.AddMember("row", row_number, Alloc);
  location.AddMember("column", col_number, Alloc);

  auto &DE = Context.getDiagnostics();
  unsigned already_has_attr = DE.getCustomDiagID(
      DiagnosticsEngine::Warning,
      "%select{Flag|Helper}0 '%1' is already defined for this declaration");

  if (named->hasAttrs()) {
    for (auto attr : named->getAttrs()) {
      if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
        Value name(flag->getName().data(), flag->getNameLength(), Alloc);
        if (flags.HasMember(flag->getName().data())) {
          DE.Report(flag->getLocation(), already_has_attr)
              << 0 << flag->getName();
          continue;
        }
        flags.AddMember(name, flag->getVal(), Alloc);
      }
      if (auto helper = dyn_cast<GhostHelperAttr>(attr)) {
        if (helpers.HasMember(helper->getName().data())) {
          DE.Report(helper->getLocation(), already_has_attr)
              << 1 << helper->getName();
          continue;
        }
        auto val = engine.getHelperValue(helper, Context);
        helpers.AddMember(Value(helper->getName().data(), Alloc), val, Alloc);
      }
    }
  }

  val.AddMember("name", name, Alloc);
  val.AddMember("location", location, Alloc);
  val.AddMember("flags", flags, Alloc);
  val.AddMember("helpers", helpers, Alloc);
}
