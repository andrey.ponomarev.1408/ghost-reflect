﻿/*!
 * \file      definitions.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "clang/AST/Attr.h"
#include "clang/AST/Decl.h"
#include "clang/AST/QualTypeNames.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/Dynamic/VariantValue.h"
#include "clang/Lex/Lexer.h"

#include <boost/filesystem.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/type_index.hpp>
#include <boost/type_index/runtime_cast.hpp>
#include <boost/type_index/runtime_cast/register_runtime_class.hpp>

#include <rapidjson/document.h>

#include <iostream>
#include <variant>

#ifndef NDEBUG
#define Assert(Expr, Msg) __M_Assert(#Expr, Expr, __FILE__, __LINE__, Msg)
#else
#define Assert(Expr, Msg) ;
#endif

#define AbortIfNot(Expr, Msg) __M_Assert(#Expr, Expr, __FILE__, __LINE__, Msg)

void __M_Assert(const char *expr_str, bool expr, const char *file, int line,
                const char *msg);

void __M_Assert(const char *expr_str, bool expr, const char *file, int line,
                const std::string &msg);

#ifndef RTTI_ENABLED
#if defined(__clang__)
#if __has_feature(cxx_rtti)
#define RTTI_ENABLED 1
#else
#define RTTI_ENABLED 0
#endif
#elif defined(__GNUG__)
#if defined(__GXX_RTTI)
#define RTTI_ENABLED 1
#else
#define RTTI_ENABLED 0
#endif
#elif defined(_MSC_VER)
#if defined(_CPPRTTI)
#define RTTI_ENABLED 1
#else
#define RTTI_ENABLED 0
#endif
#endif
#endif

using namespace clang;
using namespace clang::ast_matchers;
using namespace clang::TypeName;

namespace fs = boost::filesystem;

using boost::typeindex::runtime_cast;
using boost::typeindex::runtime_pointer_cast;

std::vector<std::string> FilterAttributes(const AttrVec &attrs);

template <typename T, typename... Ts>
std::ostream &operator<<(std::ostream &os, const std::variant<T, Ts...> &var) {
  std::visit([&os](const auto &v) { os << v; }, var);
  return os;
}

template <typename First, typename Second>
std::ostream &operator<<(std::ostream &os,
                         const std::pair<First, Second> &pair) {
  os << pair.first << "|" << pair.second;
}

template <typename Base, typename... Args>
std::enable_if_t<(std::is_base_of_v<Base, Args> && ...), const Base &>
get_base(const std::variant<Args...> &val) {
  return std::visit([](const Base &v) { return v; }, val);
}

template <typename Base, typename... Args>
std::enable_if_t<(std::is_base_of_v<Base, Args> && ...), Base &&>
get_base(std::variant<Args...> &&val) {
  return std::visit([](Base &&v) { return v; }, val);
}

struct GhostFunctionCallExpr {
  enum Type : unsigned {
    method,
    function,
    object,
    op_call,
    conversion,
    constructor
  } type;

  std::vector<QualType> arg_types;
  QualType return_type;
  std::string expr;
};

enum class AccessorType : unsigned { getter, setter };

std::string to_string(AccessorType type);

struct Accessor {
  AccessorType type;
  std::string call_expr;
  std::string original_expr;
  std::vector<std::string> signatures;
};

class ReflectionEngine;

struct ID {
public:
  using uint = std::conditional_t<sizeof(void *) == sizeof(uint32_t), uint32_t,
                                  uint64_t>;
  uint value_;

public:
  ID() : ID(nullptr) {}

  ~ID() = default;

  ID(const ID &) = default;
  ID(ID &&) = default;

  ID &operator=(const ID &) = default;
  ID &operator=(ID &&) = default;

  ID(const void *ptr) : value_(reinterpret_cast<uint>(ptr)) {}
  operator uint() const { return value_; }
};

class ReflectionCallback : public MatchFinder::MatchCallback {
public:
  explicit ReflectionCallback(ReflectionEngine &engine) : engine_(engine) {}
  virtual ~ReflectionCallback() = default;

protected:
  ReflectionEngine &engine_;

private:
  virtual void run(const MatchFinder::MatchResult &result) = 0;
};

struct LocationInfo {
public:
  fs::path file_;

public:
  LocationInfo() = default;

  ~LocationInfo() = default;

  LocationInfo(const LocationInfo &) = default;
  LocationInfo(LocationInfo &&) = default;

  LocationInfo &operator=(const LocationInfo &) = default;
  LocationInfo &operator=(LocationInfo &&) = default;

  LocationInfo(const fs::path &file) : file_(file) {}

  LocationInfo(fs::path &&file) : file_(file) {}

  friend std::ostream &operator<<(std::ostream &os, const LocationInfo &info);
};

enum class DeclType { Var, Field, Function, Method, Record, Enum, Invalid };

struct DeclInfo {
public:
  DeclType decl_type_ = DeclType::Invalid;
  fs::path file_;
  std::string name_;
  std::string short_name_;
  std::unordered_map<std::string, bool> flags_;
  std::unordered_map<std::string, std::optional<GhostFunctionCallExpr>>
      helpers_;

public:
  DeclInfo() = default;

  ~DeclInfo() = default;

  DeclInfo(const DeclInfo &) = default;
  DeclInfo(DeclInfo &&) = default;

  DeclInfo &operator=(const DeclInfo &) = default;
  DeclInfo &operator=(DeclInfo &&) = default;

  DeclInfo(const NamedDecl *decl);
};

struct VarDeclInfo : public DeclInfo {
public:
  std::string type_;
  std::string init_;
  std::vector<Accessor> accessors_;

public:
  VarDeclInfo() = default;

  ~VarDeclInfo() = default;

  VarDeclInfo(const VarDeclInfo &) = default;
  VarDeclInfo(VarDeclInfo &&) = default;

  VarDeclInfo &operator=(const VarDeclInfo &) = default;
  VarDeclInfo &operator=(VarDeclInfo &&) = default;

  VarDeclInfo(const VarDecl *decl);

protected:
  VarDeclInfo(const FieldDecl *decl);
};

struct FieldDeclInfo : public VarDeclInfo {
public:
  AccessSpecifier access_specifier_;

public:
  FieldDeclInfo() = default;

  ~FieldDeclInfo() = default;

  FieldDeclInfo(const FieldDeclInfo &) = default;
  FieldDeclInfo(FieldDeclInfo &&) = default;

  FieldDeclInfo &operator=(const FieldDeclInfo &) = default;
  FieldDeclInfo &operator=(FieldDeclInfo &&) = default;

  FieldDeclInfo(const FieldDecl *decl);
};

// using ParamVarDecl = std::decay_t<std::remove_pointer_t<
// decltype((std::unique_ptr<FunctionDecl>(nullptr))->getParamDecl(0)) >>;

struct TemplatedInfo {
public:
  //                       name - type
  //  struct SpecialiazationParam {
  //    std::string param_;
  //  };
  //  std::unordered_map<std::string, std::string> template_params_;
  //  std::vector<std::string> template_specialization_params_;

public:
  TemplatedInfo() = default;

  ~TemplatedInfo() = default;

  TemplatedInfo(const TemplatedInfo &) = default;
  TemplatedInfo(TemplatedInfo &&) = default;

  TemplatedInfo &operator=(const TemplatedInfo &) = default;
  TemplatedInfo &operator=(TemplatedInfo &&) = default;

  TemplatedInfo(const FunctionDecl *fn);
  TemplatedInfo(const RecordDecl *record);
};

struct FunctionDeclInfo : public DeclInfo, public TemplatedInfo {
public:
  std::string name_;
  std::string call_expr_;
  std::string return_type_;
  std::vector<VarDeclInfo> args_;

public:
  FunctionDeclInfo() = default;

  ~FunctionDeclInfo() = default;

  FunctionDeclInfo(const FunctionDeclInfo &) = default;
  FunctionDeclInfo(FunctionDeclInfo &&) = default;

  FunctionDeclInfo &operator=(const FunctionDeclInfo &) = default;
  FunctionDeclInfo &operator=(FunctionDeclInfo &&) = default;

  FunctionDeclInfo(const DeclInfo &decl_info) : DeclInfo(decl_info) {}

  FunctionDeclInfo(DeclInfo &&decl_info) : DeclInfo(decl_info) {}

  FunctionDeclInfo(const FunctionDecl *decl);
};

struct EnumDeclInfo : public DeclInfo {
public:
  std::string underlying_type_;
  std::vector<std::string> values_;

public:
  EnumDeclInfo() = default;

  ~EnumDeclInfo() = default;

  EnumDeclInfo(const EnumDeclInfo &) = default;
  EnumDeclInfo(EnumDeclInfo &&) = default;

  EnumDeclInfo &operator=(const EnumDeclInfo &) = default;
  EnumDeclInfo &operator=(EnumDeclInfo &&) = default;

  EnumDeclInfo(const EnumDecl *decl);
};

struct RecordDeclInfo : public DeclInfo, public TemplatedInfo {
public:
  //  std::vector<ID> parent_ids_;
  bool is_union;
  std::vector<FieldDeclInfo> fields_;
  std::vector<FunctionDeclInfo> methods_;
  std::vector<std::unique_ptr<Decl>> declarations_;

public:
  RecordDeclInfo() = default;

  ~RecordDeclInfo() = default;

  RecordDeclInfo(const RecordDeclInfo &) = default;
  RecordDeclInfo(RecordDeclInfo &&) = default;

  RecordDeclInfo &operator=(const RecordDeclInfo &) = default;
  RecordDeclInfo &operator=(RecordDeclInfo &&) = default;

  RecordDeclInfo(const RecordDecl *decl);

  //  RecordDeclInfo(const DeclInfo &decl_info, const DeclContextInfo &context)
  //      : DeclInfo(decl_info), DeclContextInfo(context) {}

  //  RecordDeclInfo(DeclInfo &&decl_info, DeclContextInfo &&context)
  //      : DeclInfo(decl_info), DeclContextInfo(context) {}
};

void handleCommon(const NamedDecl *named, rapidjson::Value &val,
                  ReflectionEngine &engine);

// static DeclarationMatcher AttributeRecordMatcher =
//    cxxRecordDecl(isSameOrDerivedFrom("meta::Attribute")).bind("id");

// static TypeMatcher AttributeType =
// hasUnqualifiedDesugaredType(recordType(hasDeclaration(AttributeRecordMatcher)));

// static DeclarationMatcher AttributeAliasMatcher =
//    typedefNameDecl(hasType(AttributeType)).bind("id");

// static DeclarationMatcher AttributeMatcher =
//    anyOf(AttributeRecordMatcher, AttributeAliasMatcher);

namespace clang {
namespace ast_matchers {

AST_MATCHER_P(Decl, hasFlagRecurse, StringRef, name) {

  std::string name_ref;
  if (auto named = dyn_cast<NamedDecl>(&Node))
    name_ref = named->getNameAsString();

  if (Node.hasAttrs()) {
    for (const auto &attr : Node.getAttrs()) {
      if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
        if (flag->getName() == name)
          return flag->getVal();
      }
    }
  }

  auto parent = Node.getDeclContext();
  while (parent and !isa<TranslationUnitDecl>(parent)) {

    if (auto decl = dyn_cast<Decl>(parent)) {
      if (decl->hasAttrs()) {

        for (const auto &attr : decl->getAttrs()) {
          if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
            if (flag->getName() == name)
              return flag->getVal();
          }
        }
      }
    }
    parent = parent->getParent();
  }

  return false;
}

AST_MATCHER_P(Decl, hasFlag, StringRef, name) {

  if (Node.hasAttrs()) {

    for (const auto &attr : Node.getAttrs()) {
      if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
        if (flag->getName() == name)
          return flag->getVal();
      }
    }
  }
  return false;
}

} // namespace ast_matchers
} // namespace clang

// static DeclarationMatcher invalidReflection =
//    decl(anyOf(isPrivate(), isProtected()), hasFlag("reflect"),
//    unless(fieldDecl())).bind("id");

static DeclarationMatcher isReflected =
    decl(isExpansionInMainFile(),
         anyOf(hasFlagRecurse("reflect"),
               hasDescendant(decl(hasFlag("reflect")))),
         unless(anyOf(isPrivate(), isProtected())))
        .bind("id");
