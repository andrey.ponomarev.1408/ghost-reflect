/*!
 * \author Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \file visitor.hpp
 * \date 09.21.2019
 * \copyright This file is part of Ghost-Reflect Project
 *             License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 *
 * \brief This file implements ASTVisitor
 */
#pragma once

#include "definitions.hpp"
#include "helper_decl_finder.hpp"
#include "reflect_callback.hpp"

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"

#include <rapidjson/document.h>
#include <rapidjson/pointer.h>

#include <stack>

// Need to redefine TRY_TO, because clang undefines it
#define TRY_TO(CALL_EXPR)                                                      \
  do {                                                                         \
    if (!getDerived().CALL_EXPR)                                               \
      return false;                                                            \
  } while (false)

template <typename Derived>
class SmartVisitor : public RecursiveASTVisitor<Derived> {
public:
  Derived &getDerived() { return *static_cast<Derived *>(this); }

  // Write own versions of Traverse##CLASS##Decl for sipporting PostVisit
#define ABSTRACT_DECL(DECL)
#define DECL(CLASS, BASE)                                                      \
  bool Traverse##CLASS##Decl(CLASS##Decl *D) {                                 \
    assert(!getDerived().shouldTraversePostOrder() and                         \
           "SmartVisitor is incompatible with PostOrder Traversal");           \
    bool ReturnValue = RecursiveASTVisitor<Derived>::Traverse##CLASS##Decl(D); \
    TRY_TO(PostWalkUpFrom##CLASS##Decl(D));                                    \
    return ReturnValue;                                                        \
  }
#include "clang/AST/DeclNodes.inc"

  // Define WalkUpFrom*() and empty Visit*() for all Decl classes.
  bool PostWalkUpFromDecl(Decl *D) { return getDerived().VisitDecl(D); }
  bool PostVisitDecl(Decl *D) { return true; }
#define DECL(CLASS, BASE)                                                      \
  bool PostWalkUpFrom##CLASS##Decl(CLASS##Decl *D) {                           \
    TRY_TO(PostWalkUpFrom##BASE(D));                                           \
    TRY_TO(PostVisit##CLASS##Decl(D));                                         \
    return true;                                                               \
  }                                                                            \
  bool PostVisit##CLASS##Decl(CLASS##Decl *D) { return true; }
#include "clang/AST/DeclNodes.inc"
};

rapidjson::Pointer
Reflect(NamespaceDecl *ns, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
Reflect(RecordDecl *record, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
Reflect(FieldDecl *field, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
Reflect(EnumDecl *en, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
Reflect(CXXMethodDecl *fn, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
Reflect(VarDecl *var, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix = rapidjson::Pointer());

rapidjson::Pointer
LightReflect(NamespaceDecl *ns, ASTContext &Context, rapidjson::Document &doc,
             std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
             rapidjson::Pointer prefix = rapidjson::Pointer());
rapidjson::Pointer
LightReflect(RecordDecl *record, ASTContext &Context, rapidjson::Document &doc,
             std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
             rapidjson::Pointer prefix = rapidjson::Pointer());

enum class reflect_kind { reflect_true, reflect_false, unknown };

reflect_kind isReflected(NamedDecl *decl);

extern std::string Prefix;
extern std::string Postfix;

extern std::string WorkingDir;
extern std::string OutputFile;

extern HelperCallback *helper_cb;

class GhostVisitor : public SmartVisitor<GhostVisitor> {
public:
  using Base = SmartVisitor<GhostVisitor>;

  GhostVisitor(ASTContext &context)
      : Context(context), SM(context.getSourceManager()) {
    visited_decls_.reserve(10000);
    visited_decls_.max_load_factor(0.8f);
    reflected_.reserve(100);
  }

  bool TraverseDecl(Decl *decl) {
    if (auto record = dyn_cast_or_null<RecordDecl>(decl)) {
      auto &SM = record->getASTContext().getSourceManager();
      if (SM.isInMainFile(record->getLocation()) and
          record == record->getDefinition() and !record->isDependentType() and
          !record->isDependentContext() and
          !isa<ClassTemplateSpecializationDecl>(record))
        Base::TraverseDecl(decl);
    } else if (auto ns = dyn_cast_or_null<NamespaceDecl>(decl)) {
      auto &SM = ns->getASTContext().getSourceManager();
      if (SM.isInMainFile(ns->getLocation()))
        Base::TraverseDecl(decl);
    }
    else if (auto fn = dyn_cast_or_null<FunctionDecl>(decl)) {
      if (!fn->isDependentContext()) {
        if (auto dtor = dyn_cast_or_null<CXXDestructorDecl>(fn))
          return WalkUpFromCXXDestructorDecl(dtor);
        else if (auto conversion = dyn_cast_or_null<CXXConversionDecl>(fn))
          return WalkUpFromCXXConversionDecl(conversion);
        else if (auto ctor = dyn_cast_or_null<CXXConstructorDecl>(fn))
          return WalkUpFromCXXConstructorDecl(ctor);
        else if (auto method = dyn_cast_or_null<CXXMethodDecl>(fn))
          return WalkUpFromCXXMethodDecl(method);
        else
          return WalkUpFromFunctionDecl(fn);
      return true;
      }
    } else {
      Base::TraverseDecl(decl);
    }

    return true;
  }

  rapidjson::Document &GetDocument() { return doc_; }

  bool VisitDecl(Decl *decl) {
    most_recent_decl_ = nullptr;
    return true;
  }

  bool VisitNamespaceDecl(NamespaceDecl *ns) {

    auto parent = refl_top();
    reflect_kind is_reflected = isReflected(ns);
    refl_push(std::make_pair(
        is_reflected == reflect_kind::reflect_false
            ? false
            : parent.first or is_reflected == reflect_kind::reflect_true,
        rapidjson::Pointer()));
    if (refl_top().first) {
      most_recent_decl_ = ns;
      rapidjson::Pointer parent =
          PropagateParents(Context, ns, doc_, visited_decls_);
      refl_top_ref().second =
          Reflect(ns, Context, doc_, visited_decls_, parent);
    }

    return true;
  }
  bool PostVisitNamespaceDecl(NamespaceDecl *ns) {
    refl_pop();
    return true;
  }

  bool VisitRecordDecl(RecordDecl *record) {

    auto parent = refl_top();
    reflect_kind is_reflected = isReflected(record);
    refl_push(std::make_pair(
        is_reflected == reflect_kind::reflect_false
            ? false
            : parent.first or is_reflected == reflect_kind::reflect_true,
        rapidjson::Pointer()));
    if (parent.first) {
      most_recent_decl_ = record;
      parent.second =
          PropagateParents(Context, record, doc_, visited_decls_);
      refl_top_ref().second =
          Reflect(record, Context, doc_, visited_decls_, parent.second);
    }

    return true;
  }

  bool VisitEnumDecl(EnumDecl *en) {
    auto parent = refl_top();
    if (parent.first and en == en->getDefinition()) {
      most_recent_decl_ = en;
      parent.second = PropagateParents(Context, en, doc_, visited_decls_);
      refl_top_ref().second = 
        Reflect(en, Context, doc_, visited_decls_, parent.second);
    }
    return true;
  }

  bool PostVisitRecordDecl(RecordDecl *record) {
    refl_pop();
    return true;
  }

  bool VisitFieldDecl(FieldDecl *field) {
    auto parent = refl_top();
    if (parent.first) {
      most_recent_decl_ = field;
      Reflect(field, Context, doc_, visited_decls_, parent.second);
    }
    return true;
  }
  bool VisitCXXMethodDecl(CXXMethodDecl* method) {
    auto parent = refl_top();
    if (parent.first and !dyn_cast<CXXDestructorDecl>(method)) {
      most_recent_decl_ = method;
      Reflect(method, Context, doc_, visited_decls_, parent.second);
    }
    return true;
  }

private:
  std::pair<bool, rapidjson::Pointer> refl_top() const {
    return reflected_.empty() ? std::make_pair(false, rapidjson::Pointer())
                              : reflected_.back();
  }
  std::pair<bool, rapidjson::Pointer> &refl_top_ref() {
    return reflected_.back();
  }
  void refl_push(std::pair<bool, rapidjson::Pointer> val) {
    reflected_.push_back(val);
  }
  void refl_pop() { reflected_.pop_back(); }

  ASTContext &Context;
  SourceManager &SM;

  NamedDecl *most_recent_decl_ = nullptr;
  std::unordered_map<NamedDecl *, rapidjson::Pointer> visited_decls_;
  rapidjson::Document doc_;
  std::vector<std::pair<bool, rapidjson::Pointer>> reflected_;
};

class GhostConsumer : public clang::ASTConsumer {
public:
  explicit GhostConsumer(ASTContext &Context);

  virtual void HandleTranslationUnit(clang::ASTContext &Context);

private:
  GhostVisitor Visitor;
};

class GhostReflectAction : public clang::ASTFrontendAction {
public:
  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance &Compiler,
                    llvm::StringRef InFile) override;
};

#undef TRY_TO
