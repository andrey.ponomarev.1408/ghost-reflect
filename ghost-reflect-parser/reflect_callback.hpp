/*!
 * \author Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \file reflect_callback.hpp
 * \date 09.30.2019
 * \copyright This file is part of Ghost-Reflect Project
 *             License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 *
 * \brief Declares Reflect interface
 */
#pragma once

#include "definitions.hpp"

#include <rapidjson/pointer.h>

rapidjson::Pointer PropagateParents(
    ASTContext &Context, NamedDecl *decl, rapidjson::Document &doc,
    std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls);
