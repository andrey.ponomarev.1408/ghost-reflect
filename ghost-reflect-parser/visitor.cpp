/*!
 * \author Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \file visitor.cpp
 * \date 09.21.2019
 * \copyright This file is part of Ghost-Reflect Project
 *             License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 *
 * \brief This file contains implementation for visitors
 */
#include "visitor.hpp"
#include "definitions.hpp"
#include "generatefunctioncall.hpp"
#include "helper_decl_finder.hpp"

#include <rapidjson/prettywriter.h>
#include <rapidjson/stream.h>

#include <locale>
#include <unordered_set>
#include <fstream>

using namespace rapidjson;

std::string Prefix = "";
std::string Postfix = "";
std::string OutputFile = "";
std::string WorkingDir = "";
HelperCallback *helper_cb = nullptr;

static const char *to_str(AccessSpecifier acc) {
  static const char lookup[4][10]{"public", "protected", "private", "none"};
  return lookup[acc];
};

static void handleCommon(NamedDecl *named, Value &val, ASTContext &Context,
                         Document::AllocatorType &Alloc) {
  auto &SM = Context.getSourceManager();

  val.SetObject();

  Value location(kObjectType);
  Value flags(kObjectType);
  Value helpers(kObjectType);

  auto file_id = SM.getFileID(named->getLocation());
  auto file_offset = SM.getFileOffset(named->getLocation());
  auto col_number = SM.getColumnNumber(file_id, file_offset);
  auto row_number = SM.getLineNumber(file_id, file_offset);

  auto path_str =
      fs::relative(SM.getFilename(named->getLocation()).data(), WorkingDir)
          .string();
  location.AddMember("file", Value(path_str.c_str(), path_str.size(), Alloc),
                     Alloc);
  location.AddMember("file_offset", file_offset, Alloc);
  location.AddMember("row", row_number, Alloc);
  location.AddMember("column", col_number, Alloc);

  auto &DE = Context.getDiagnostics();
  unsigned already_has_attr = DE.getCustomDiagID(
      DiagnosticsEngine::Warning,
      "%select{Flag|Helper}0 '%1' is already defined for this declaration");

  if (named->hasAttrs()) {
    for (auto attr : named->getAttrs()) {
      if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
        Value name(flag->getName().data(), flag->getNameLength(), Alloc);
        if (flags.HasMember(flag->getName().data())) {
          DE.Report(flag->getLocation(), already_has_attr)
              << 0 << flag->getName();
          continue;
        }
        flags.AddMember(name, flag->getVal(), Alloc);
      }
      if (auto helper = dyn_cast<GhostHelperAttr>(attr)) {
        if (helpers.HasMember(helper->getName().data())) {
          DE.Report(helper->getLocation(), already_has_attr)
              << 1 << helper->getName();
          continue;
        }
        assert(helper_cb);
        auto opt =
            helper_cb->getHelperValue(helper, Context, Alloc, Prefix, Postfix);
        if (!opt.has_value()) {
          // error
          continue;
        }
        auto& val = opt.value();
        helpers.AddMember(
            Value(val.first.c_str(), val.first.size(), Alloc),
            val.second, Alloc);
      }
    }
  }

  val.AddMember("name", Value(named->getNameAsString().c_str(), Alloc), Alloc);

  val.AddMember("location", location, Alloc);
  val.AddMember("flags", flags, Alloc);
  val.AddMember("helpers", helpers, Alloc);
  val.AddMember("access", Value(to_str(named->getAccess()), Alloc), Alloc);
}

static void
getUniqueBases(std::unordered_set<const CXXRecordDecl *> &visited_bases,
               std::vector<const CXXRecordDecl *> &out,
               const CXXRecordDecl *Record) {
  if (!visited_bases.count(Record)) {
    for (const auto &base : Record->bases()) {
      const RecordType *Ty = base.getType()->getAs<RecordType>();
      if (!Ty) {
        continue;
      }

      CXXRecordDecl *Base =
          cast_or_null<CXXRecordDecl>(Ty->getDecl()->getDefinition());
      if (!Base)
        continue;

      if (!visited_bases.count(Base)) {
        getUniqueBases(visited_bases, out, Base);
      }
    }

    visited_bases.insert(Record);
    out.push_back(Record);
  }
}

std::vector<const CXXRecordDecl *> getUniqueBases(const CXXRecordDecl *Record) {

  std::unordered_set<const CXXRecordDecl *> visited_bases;
  std::vector<const CXXRecordDecl *> out;
  for (const auto &base : Record->bases()) {
    const RecordType *Ty = base.getType()->getAs<RecordType>();
    if (!Ty) {
      continue;
    }

    CXXRecordDecl *Base =
        cast_or_null<CXXRecordDecl>(Ty->getDecl()->getDefinition());
    if (!Base)
      continue;

    if (!visited_bases.count(Base)) {
      getUniqueBases(visited_bases, out, Base);
    }
  }
  return out;
}

std::unique_ptr<ASTConsumer>
GhostReflectAction::CreateASTConsumer(CompilerInstance &Compiler,
                                      llvm::StringRef InFile) {
  return std::unique_ptr<clang::ASTConsumer>(
      new GhostConsumer(Compiler.getASTContext()));
}

GhostConsumer::GhostConsumer(ASTContext &Context) : Visitor(Context) {}

void GhostConsumer::HandleTranslationUnit(ASTContext &Context) {
  Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  StringBuffer buf;
#ifdef NDEBUG
  Writer<StringBuffer> writer(buf);
#else
  PrettyWriter<StringBuffer> writer(buf);
#endif
  Visitor.GetDocument().Accept(writer);
  if (OutputFile.empty())
    std::cout << buf.GetString();
  else {
    std::ofstream file(WorkingDir + '/' + OutputFile);
    if (file.is_open()) {
      file << buf.GetString();
      if (file.fail() or file.bad())
        std::cerr << "Failed to write to file " << OutputFile;
    } else {
      std::cerr << "Failed to open file " << OutputFile;
    }
  }
}

rapidjson::Pointer
Reflect(NamespaceDecl *ns, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix) {
  auto pointer = LightReflect(ns, Context, doc, visited_decls, prefix);

  return pointer;
}
rapidjson::Pointer
Reflect(RecordDecl *record, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer prefix) {
  using namespace rapidjson;
  auto pointer = LightReflect(record, Context, doc, visited_decls, prefix);

  rapidjson::Value &val_p = *pointer.Get(doc);
  auto &alloc = doc.GetAllocator();

  val_p.AddMember("fields", Value(kArrayType), alloc);
  val_p.AddMember("methods", Value(kArrayType), alloc);
  val_p.AddMember("variables", Value(kArrayType), alloc);

  val_p.AddMember("records", Value(kArrayType), alloc);

  return pointer;
}
rapidjson::Pointer
Reflect(EnumDecl *en, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer pointer) {

  static const std::string container_name = "enums";

  en = en->getDefinition();

  pointer = pointer.Append(container_name.c_str(), container_name.size());
  Value &container = pointer.Create(doc);
  if (container.IsNull())
    container.SetArray();
  pointer = pointer.Append(container.Size());

  using namespace rapidjson;
  auto &alloc = doc.GetAllocator();
  Value val;
  handleCommon(en, val, Context, doc.GetAllocator());
  Value enumerators(kArrayType);
  size_t size = 0;
  for (auto en_val : en->enumerators()) {
    enumerators.PushBack(
        Value(en_val->getName().data(), en_val->getName().size(), alloc),
        alloc);
    ++size;
  }
  val.AddMember("enumerators", enumerators, alloc);
  val.AddMember("size", size, alloc);

  container.PushBack(val, alloc);

  return rapidjson::Pointer();
}

void AddAccessors(const FieldDecl *field, rapidjson::Value &val,
                  rapidjson::Document::AllocatorType &alloc) {
  using namespace rapidjson;

  val.AddMember("getter", Value(), alloc);
  val.AddMember("setter", Value(), alloc);

  Value &getter = val["getter"];
  Value &setter = val["setter"];

  auto &Context = field->getASTContext();
  auto &DE = Context.getDiagnostics();
  const unsigned invalid_accessor =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid accessor. Should be a public "
                         "member function or function/functor "
                         "with signature return_type(object_reference) or "
                         "return_type(object_reference, value_to_set)");
  const unsigned already_defined = DE.getCustomDiagID(
      clang::DiagnosticsEngine::Error,
      "%select{Getter|Setter}0 was already defined by previous accessor.");

  if (auto property = field->getAttr<GhostPropertyAttr>()) {

    std::unordered_map<AccessorType, Accessor> generated_acc;

    for (const auto &acc : property->accessors()) {

      auto tmp = generateAccessors(acc, field, Context, Prefix, Postfix);

      if (tmp.empty()) {
        DE.Report(acc->getExprLoc(), invalid_accessor);
        return;
      }

      for (auto &&val :
           boost::make_iterator_range(std::make_move_iterator(tmp.begin()),
                                      std::make_move_iterator(tmp.end()))) {
        auto result = generated_acc.emplace(val);
        if (!result.second) {
          DE.Report(acc->getExprLoc(), already_defined) << unsigned(val.first);
          return;
        }
      }
    }

    auto set_accessor = [](Value &val, const Accessor &acc,
                           Document::AllocatorType &alloc) {
      if (!val.IsObject())
        val.SetObject();
      val.AddMember("call_expr", Value(acc.call_expr.c_str(), alloc), alloc);
      val.AddMember("original_expr", Value(acc.original_expr.c_str(), alloc),
                    alloc);
      Value signatures(kArrayType);
      for (auto &sig : acc.signatures) {
        signatures.PushBack(Value(sig.c_str(), alloc), alloc);
      }
      val.AddMember("signatures", signatures, alloc);
    };

    for (auto &acc : generated_acc) {
      if (acc.first == AccessorType::getter)
        set_accessor(getter, acc.second, alloc);
      else
        set_accessor(setter, acc.second, alloc);
    }
  } else if (field->getAccess() == AS_public) {

    auto field_name = field->getNameAsString();

    auto record_type = QualType(field->getParent()->getTypeForDecl(), 0);
    auto crecord_type = record_type;
    crecord_type.addConst();
    auto record_lref = Context.getLValueReferenceType(record_type);
    auto record_rref = Context.getRValueReferenceType(record_type);
    auto record_clref = Context.getLValueReferenceType(record_type);

    auto record_lref_str = getFullyQualified(record_lref, Context);
    auto record_rref_str = getFullyQualified(record_rref, Context);
    auto record_clref_str = getFullyQualified(record_clref, Context);

    auto field_type = field->getType();
    if (field_type->isReferenceType())
      field_type = field_type->getPointeeType();
    auto cfield_type = field_type;
    cfield_type.addConst();

    auto field_lref = Context.getLValueReferenceType(field_type);
    auto field_rref = Context.getRValueReferenceType(field_type);
    auto field_clref = Context.getLValueReferenceType(cfield_type);

    auto field_lref_str = getFullyQualified(field_lref, Context);
    auto field_rref_str = getFullyQualified(field_rref, Context);
    auto field_clref_str = getFullyQualified(field_clref, Context);

    auto member_ptr_signature = "&" + getFullyQualified(record_type, Context) +
                                "::" + field->getNameAsString();

    getter.SetObject();
    getter.AddMember(
        "call_expr",
        Value((Prefix + "_0" + Postfix + '.' + field_name).c_str(), alloc),
        alloc);
    getter.AddMember("original_expr",
                     Value(member_ptr_signature.c_str(), alloc), alloc);
    Value getter_signatures(kArrayType);
    getter_signatures.PushBack(
        Value((field_clref_str + '(' + record_clref_str + ')').c_str(), alloc),
        alloc);
    getter_signatures.PushBack(
        Value((field_lref_str + '(' + record_lref_str + ')').c_str(), alloc),
        alloc);
    getter.AddMember("signatures", getter_signatures, alloc);

    // For setter we need to determine if it is assignable at all
    auto big_five = getBigFive(field_type);
    if (big_five.test(BigFive::copy_assignable) or
        big_five.test(BigFive::move_assignable)) {

      setter.SetObject();
      setter.AddMember("call_expr",
                       Value((Prefix + "_0" + Postfix + '.' + field_name + '=' +
                              Prefix + "_1" + Postfix)
                                 .c_str(),
                             alloc),
                       alloc);
      setter.AddMember("original_expr",
                       Value(member_ptr_signature.c_str(), alloc), alloc);
      Value setter_signature(kArrayType);

      if (big_five.test(BigFive::copy_assignable))
        setter_signature.PushBack(
            Value((field_lref_str + '(' + record_lref_str + ", " +
                   field_clref_str + ')')
                      .c_str(),
                  alloc),
            alloc);

      if (big_five.test(BigFive::move_assignable))
        setter_signature.PushBack(
            Value((field_lref_str + '(' + record_lref_str + ", " +
                   field_rref_str + ')')
                      .c_str(),
                  alloc),
            alloc);

      setter.AddMember("signatures", setter_signature, alloc);
    }
  }
}

rapidjson::Pointer
Reflect(FieldDecl *field, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer pointer) {
  static const std::string container_name = "fields";

  pointer = pointer.Append(container_name.c_str(), container_name.size());
  Value &container = pointer.Create(doc);
  if (container.IsNull())
    container.SetArray();
  pointer = pointer.Append(container.Size());

  using namespace rapidjson;
  auto &alloc = doc.GetAllocator();
  Value val;
  handleCommon(field, val, Context, doc.GetAllocator());
  std::string type = getFullyQualified(field->getType(), Context);
  val.AddMember("type", Value(type.c_str(), alloc), alloc);
  AddAccessors(field, val, alloc);

  container.PushBack(val, alloc);

  return pointer;
}
rapidjson::Pointer
Reflect(CXXMethodDecl *method, ASTContext &Context, rapidjson::Document &doc,
        std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
        rapidjson::Pointer pointer) {

  static const std::string container_name = "methods";
  pointer = pointer.Append(container_name.c_str(), container_name.size());
  Value& container = pointer.Create(doc);
  if (container.IsNull())
    container.SetArray();
  pointer = pointer.Append(container.Size());

  using namespace rapidjson;
  auto& alloc = doc.GetAllocator();
  Value val;
  handleCommon(method, val, Context, doc.GetAllocator());
  std::string type = getFullyQualified(method->getType(), Context);
  val.AddMember("is_static", method->isStatic(), alloc);
  val.AddMember("type", Value(type.c_str(), alloc), alloc);
  val.AddMember("is_ctor", !!dyn_cast_or_null<CXXConstructorDecl>(method), alloc);

  container.PushBack(val, alloc);

  return pointer;
}

Pointer Reflect(VarDecl *var, ASTContext &Context, Document &doc,
                std::unordered_map<NamedDecl *, Pointer> &visited_decls,
                Pointer prefix) {
  std::cout << "Trying to reflect var " << var->getNameAsString() << std::endl;
  return rapidjson::Pointer();
}

void HandleCommon(const NamedDecl *named, rapidjson::Value &val,
                  rapidjson::Document::AllocatorType &alloc) {
  auto &Context = named->getASTContext();
  auto &SM = Context.getSourceManager();

  Value location(kObjectType);
  Value flags(kObjectType);
  Value helpers(kObjectType);

  auto file_id = SM.getFileID(named->getLocation());
  auto file_offset = SM.getFileOffset(named->getLocation());
  auto col_number = SM.getColumnNumber(file_id, file_offset);
  auto row_number = SM.getLineNumber(file_id, file_offset);

  auto path =
      fs::relative(SM.getFilename(named->getLocation()).data(), WorkingDir)
          .string();
  location.AddMember("file", Value(path.c_str(), path.size(), alloc), alloc);
  location.AddMember("file_offset", file_offset, alloc);
  location.AddMember("row", row_number, alloc);
  location.AddMember("column", col_number, alloc);

  auto &DE = Context.getDiagnostics();
  unsigned already_has_attr = DE.getCustomDiagID(
      DiagnosticsEngine::Warning,
      "%select{Flag|Helper}0 '%1' is already defined for this declaration");

  if (named->hasAttrs()) {
    for (auto attr : named->getAttrs()) {
      if (auto flag = dyn_cast<GhostFlagAttr>(attr)) {
        Value name(flag->getName().data(), flag->getNameLength(), alloc);
        if (flags.HasMember(flag->getName().data())) {
          DE.Report(flag->getLocation(), already_has_attr)
              << 0 << flag->getName();
          continue;
        }
        flags.AddMember(name, flag->getVal(), alloc);
      }
      if (auto helper = dyn_cast<GhostHelperAttr>(attr)) {
        if (helpers.HasMember(helper->getName().data())) {
          DE.Report(helper->getLocation(), already_has_attr)
              << 1 << helper->getName();
          continue;
        }
        std::cerr << "Helpers are not supported, yet" << std::endl;
        //   auto val = engine.getHelperValue(helper, Context);
        //   helpers.AddMember(Value(helper->getName().data(), Alloc), val,
        //   Alloc);
      }
    }
  }

  val.AddMember("name", Value(named->getNameAsString().c_str(), alloc), alloc);

  val.AddMember("location", location, alloc);
  val.AddMember("flags", flags, alloc);
  val.AddMember("helpers", helpers, alloc);
}

rapidjson::Pointer
LightReflect(NamespaceDecl *ns, ASTContext &Context, rapidjson::Document &doc,
             std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
             rapidjson::Pointer pointer) {
  using namespace rapidjson;
  static const std::string container_name = "namespaces";

  ns = ns->getFirstDecl();

  pointer = pointer.Append(container_name.c_str(), container_name.size());
  Value &container = pointer.Create(doc);
  if (container.IsNull())
    container.SetArray();
  static std::string size_str;
  if (size_str.capacity() == 0)
    size_str.reserve(10);

  pointer = pointer.Append(container.Size());

  auto result = visited_decls.try_emplace(ns, pointer);
  if (!result.second)
    return result.first->second;

  Value val;
  handleCommon(ns, val, Context, doc.GetAllocator());
  container.PushBack(val, doc.GetAllocator());

  return result.first->second;
}
rapidjson::Pointer
LightReflect(RecordDecl *record, ASTContext &Context, rapidjson::Document &doc,
             std::unordered_map<NamedDecl *, rapidjson::Pointer> &visited_decls,
             rapidjson::Pointer pointer) {
  using namespace rapidjson;
  static const std::string container_name = "records";

  record = record->getDefinition();

  pointer = pointer.Append(container_name.c_str(), container_name.size());
  Value &container = pointer.Create(doc);
  if (container.IsNull())
    container.SetArray();
  pointer = pointer.Append(container.Size());

  auto result = visited_decls.try_emplace(record, pointer);
  if (!result.second)
    return result.first->second;

  Value val;
  handleCommon(record, val, Context, doc.GetAllocator());

  if (auto cxxrecord = dyn_cast_or_null<CXXRecordDecl>(record)) {
    auto all_bases = getUniqueBases(cxxrecord);

    Value bases_json(kArrayType);
    for (auto &base : cxxrecord->bases()) {
      Value current_base_json(kObjectType);
      std::string base_type_str =
          getFullyQualified(base.getType().getCanonicalType(), Context);
      current_base_json.AddMember("name",
                                  Value(base_type_str.c_str(),
                                        base_type_str.size(),
                                        doc.GetAllocator()),
                                  doc.GetAllocator());
      current_base_json.AddMember("is_virtual", base.isVirtual(),
                                  doc.GetAllocator());
      std::string access = to_str(base.getAccessSpecifier());
      current_base_json.AddMember(
          "access", Value(access.c_str(), access.size(), doc.GetAllocator()),
          doc.GetAllocator());
      bases_json.PushBack(current_base_json, doc.GetAllocator());
    }

    Value all_bases_json(kArrayType);
    for (auto &base : all_bases) {
      QualType base_type(base->getTypeForDecl(), 0);
      std::string base_type_str =
          getFullyQualified(base_type.getCanonicalType(), Context);
      all_bases_json.PushBack(Value(base_type_str.c_str(), base_type_str.size(),
                                    doc.GetAllocator()),
                              doc.GetAllocator());
    }

    val.AddMember("bases", bases_json, doc.GetAllocator());
    val.AddMember("all_bases", all_bases_json, doc.GetAllocator());
  }

  container.PushBack(val, doc.GetAllocator());

  return result.first->second;
}

reflect_kind isReflected(NamedDecl *decl) {
  if (!decl->hasAttrs())
    return reflect_kind::unknown;
  for (auto attr : decl->getAttrs()) {
    if (auto flag = dyn_cast_or_null<GhostFlagAttr>(attr)) {
      if (flag->getName() == "reflect")
        return flag->getVal() ? reflect_kind::reflect_true
                              : reflect_kind::reflect_false;
    }
  }
  return reflect_kind::unknown;
}
