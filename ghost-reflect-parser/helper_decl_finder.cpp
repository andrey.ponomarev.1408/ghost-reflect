
#include "helper_decl_finder.hpp"

void HelperCallback::run(
    const clang::ast_matchers::MatchFinder::MatchResult &result) {

  auto decl = result.Nodes.getNodeAs<Decl>("id");

  if (decl and decl->hasAttrs()) {
    for (auto attr : decl->getAttrs()) {
      if (auto helper_decl = dyn_cast_or_null<GhostDeclareHelperAttr>(attr)) {
        DeclareHelper(helper_decl, *result.Context);
      }
    }
  }
}

void HelperCallback::DeclareHelper(const GhostDeclareHelperAttr *helper_decl,
                                   const clang::ASTContext &Context) {
  auto &DE = Context.getDiagnostics();
  unsigned error = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "Redefinition of helper '%0' with same number of arguments: %1.");
  unsigned mismatch_mode = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "Helper '%0' is already defined as "
      "%select{'map'|'array'}1. "
      "You can't redefine it as "
      "%select{'map'|'array'}2.");
  
  HelperDeclarationAdapter new_decl(helper_decl);

  StringRef name = helper_decl->getName();
  auto same_range_pair = declared_helpers_.equal_range(name);
  auto same_range = boost::make_iterator_range(same_range_pair);

  for (auto& already_declared : same_range) {

    
    if (new_decl.storage_mode_ == HelperDeclarationAdapter::StorageMode::Array or
        already_declared.second.storage_mode_ == HelperDeclarationAdapter::StorageMode::Array) {
      DE.Report(helper_decl->getLocation(), mismatch_mode)
          << name << already_declared.second.storage_mode_ << new_decl.storage_mode_;
      return;
    }

    // Redefinition with same number of args
    if (already_declared.second.arguments_.size() ==
        new_decl.arguments_.size() and new_decl != already_declared.second) {
      DE.Report(helper_decl->getLocation(), error)
          << name << helper_decl->helpers_size();
      return;
    }
  }
  declared_helpers_.try_emplace(name.str(), std::move(new_decl));
}

std::optional<std::pair<std::string, rapidjson::Value>>
HelperCallback::getHelperValue(const GhostHelperAttr *helper,
                               const clang::ASTContext &Context,
                               rapidjson::Document::AllocatorType &alloc,
                               const std::string &prefix,
                               const std::string &postfix) {

  auto &DE = Context.getDiagnostics();
  unsigned error = DE.getCustomDiagID(
      DiagnosticsEngine::Error, "Couldn't resolve helper_decl for helper");

  unsigned cant_resolve_function_call = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "Helper argument can't be evaluated as callable and "
      "helper declaration doesn't allow non function arguments");

  using namespace rapidjson;

  clang::StringRef name = helper->getName();
  auto same_range_pair = declared_helpers_.equal_range(name);
  auto same_range = boost::make_iterator_range(same_range_pair.first, same_range_pair.second);

  const HelperDeclarationAdapter* decl = nullptr;
  
  using StorageMode = HelperDeclarationAdapter::StorageMode;
  using ArgumentMode = HelperDeclarationAdapter::ArgumentMode;
  
  // find appropriate helper declaration
  for (auto& declared : same_range) {
    auto declared_size = declared.second.arguments_.size();
    auto helper_size = helper->helpers_size();
    if (declared_size == helper_size or
        declared.second.storage_mode_ == StorageMode::Array) {
      decl = &declared.second;
      break;
    }
  }
  
  // if no was found report error
  if (!decl) {
    DE.Report(helper->getLocation(), error);
    return std::nullopt;
  }

  Value outer_value;
  outer_value.SetObject();

  Value val;

  if (decl->storage_mode_ == StorageMode::Array) {
    outer_value.AddMember("storage", Value("array", alloc), alloc);
    val.SetArray();
  } 
  else {
    outer_value.AddMember("storage", Value("array", alloc), alloc);
    val.SetObject();
  }

  switch (decl->arg_mode_)
  {
  case ArgumentMode::Functors:
    outer_value.AddMember("storage_type", Value("functors", alloc), alloc);
    break;
  case ArgumentMode::RawExprs:
    outer_value.AddMember("storage_type", Value("raw_exprs", alloc), alloc);
    break;
  case ArgumentMode::Mix:
    outer_value.AddMember("storage_type", Value("mix", alloc), alloc);
    break;
  }


  size_t count = 0;
  for (auto param : helper->helpers()) {
    Value current_helper;
    // try to generate function calls first
    if (decl->arg_mode_ == ArgumentMode::Mix or
        decl->arg_mode_ == ArgumentMode::Functors) {
      Value call_exprs =
          getFunctionCallExpr(param, Context, alloc, prefix, postfix);
      if (!call_exprs.IsNull()) {
        current_helper.SetObject();
        current_helper.AddMember("call_exprs", call_exprs, alloc);
        current_helper.AddMember(
            "raw_expr", getRawExpr(param, Context, alloc, prefix, postfix),
            alloc);
      }
    }

    if (decl->arg_mode_ == ArgumentMode::RawExprs or
      (decl->arg_mode_ == ArgumentMode::Mix and current_helper.IsNull())) {
      current_helper.SetObject();
      current_helper.AddMember(
          "raw_expr", getRawExpr(param, Context, alloc, prefix, postfix),
          alloc);
    }

    // If any of the arguments doesn't match -> just exit
    if (current_helper.IsNull())
      return std::nullopt;

    if (decl->storage_mode_ == StorageMode::Array) {
      val.PushBack(current_helper, alloc);
    } else {
      const std::string& arg_name = decl->arguments_[count];
      val.AddMember(Value(arg_name.c_str(), arg_name.size(), alloc),
                    current_helper, alloc);
    }
    ++count;
  }

  outer_value.AddMember("arguments", val, alloc);

  return std::pair<std::string, rapidjson::Value>(name.str(), std::move(outer_value));
}

rapidjson::Value HelperCallback::getFunctionCallExpr(
    const clang::Expr *expr, const clang::ASTContext &Context,
    rapidjson::Document::AllocatorType &alloc, const std::string prefix,
    const std::string &postfix) {

  using namespace rapidjson;
  Value out;
  auto call_exprs = GetFunctionCallExprs(expr, Context, prefix, postfix);

  // only if call exprs were generated
  if (!call_exprs.empty()) {
    out.SetArray();

    for (const auto &current_fn_call : call_exprs) {
      Value current_call_expr_val(kObjectType);
      current_call_expr_val.AddMember(
          "expr", Value(current_fn_call.expr.c_str(), alloc), alloc);
      std::string signature =
          getFullyQualified(current_fn_call.return_type, Context) + '(';
      for (unsigned i = 0; i < current_fn_call.arg_types.size(); ++i) {
        signature += getFullyQualified(current_fn_call.arg_types[i], Context);
        if (i + 1 != current_fn_call.arg_types.size())
          signature += ", ";
      }
      signature += ')';
      current_call_expr_val.AddMember("signature",
                                      Value(signature.c_str(), alloc), alloc);
      current_call_expr_val.AddMember("type",
        Value(getFullyQualified(current_fn_call.function_type, Context).c_str() , alloc), alloc);
      out.PushBack(current_call_expr_val, alloc);
    }
  }
  return out;
}

rapidjson::Value HelperCallback::getRawExpr(
    const clang::Expr *expr, const clang::ASTContext &Context,
    rapidjson::Document::AllocatorType &alloc, const std::string prefix,
    const std::string &postfix) {
  std::string str = getFullyQualified(expr, Context);
  return rapidjson::Value(str.data(), str.size(), alloc);
}
