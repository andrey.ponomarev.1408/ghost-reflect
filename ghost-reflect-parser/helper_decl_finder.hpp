#pragma once

#include "definitions.hpp"
#include "generatefunctioncall.hpp"

#include <rapidjson/document.h>

#include <unordered_set>
#include <optional>

static DeclarationMatcher HelperDeclMatcher =
    decl(hasAttr(attr::GhostDeclareHelper)).bind("id");

struct HelperDeclarationAdapter {
  using StorageMode = GhostDeclareHelperAttr::StorageMode;
  using ArgumentMode = GhostDeclareHelperAttr::ArgumentMode;
  
  HelperDeclarationAdapter(const GhostDeclareHelperAttr* attr) :  
    storage_mode_(attr->getStorage_mode()),
    arg_mode_(attr->getArgument_mode()),
    arguments_(attr->helpers_begin(), attr->helpers_end())
  {}
  HelperDeclarationAdapter() noexcept = default;
  HelperDeclarationAdapter(const HelperDeclarationAdapter&) = default;
  HelperDeclarationAdapter(HelperDeclarationAdapter&&) noexcept = default;
  HelperDeclarationAdapter& operator=(const HelperDeclarationAdapter&) = default;
  HelperDeclarationAdapter& operator=(HelperDeclarationAdapter&&) noexcept =default;
  
  bool operator==(const HelperDeclarationAdapter& rhs) {
    return 
        storage_mode_ == rhs.storage_mode_ and 
        arg_mode_ == rhs.arg_mode_ and 
        arguments_ == rhs.arguments_;
  }
  bool operator!=(const HelperDeclarationAdapter& rhs) {
    return !(*this == rhs);
  }
  
  StorageMode storage_mode_;
  ArgumentMode arg_mode_;
  std::vector<std::string> arguments_;
};


class HelperCallback : public MatchFinder::MatchCallback {

private:
  virtual void run(const MatchFinder::MatchResult &result) override;

  void DeclareHelper(const GhostDeclareHelperAttr *helper_decl,
                     const ASTContext &Context);

public:
  std::optional<std::pair<std::string, rapidjson::Value>>
  getHelperValue(const GhostHelperAttr *helper, const ASTContext &Context,
                 rapidjson::Document::AllocatorType &alloc,
                 const std::string &prefix, const std::string &postfix);

private:
  static rapidjson::Value
  getFunctionCallExpr(const Expr *expr, const ASTContext &Context,
                      rapidjson::Document::AllocatorType &alloc,
                      const std::string prefix, const std::string &postfix);

  static rapidjson::Value getRawExpr(const Expr *expr,
                                     const ASTContext &Context,
                                     rapidjson::Document::AllocatorType &alloc,
                                     const std::string prefix,
                                     const std::string &postfix);

  std::unordered_map<std::string, HelperDeclarationAdapter> declared_helpers_;
};
