/*!
 * \file      generateoperatorcall.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#include "generatefunctioncall.hpp"

// std::vector<GhostFunctionCallExpr>
// GetFunctionCallExprs(const FieldDecl *field,[[maybe_unused]] const Expr
// *expr,
//                     [[maybe_unused]] const ASTContext &Context) {
//  auto class_type = field->getParent()->getTypeForDecl();
//  auto member_type = field->getType();
//  member_type = member_type->isReferenceType() ? member_type->getPointeeType()
//                                               : member_type;
//  auto const_member_type = member_type;
//  const_member_type.addConst();

//  std::string name = field->getNameAsString();
//  auto bigfive = getBigFive(member_type);

//  std::vector<GhostFunctionCallExpr> out{
//      GhostFunctionCallExpr{// const getter
//                            GhostFunctionCallExpr::field,
//                            {QualType(class_type, Qualifiers::TQ::Const)},
//                            const_member_type,
//                            "_0." + name}};
//  if (!member_type.isConstQualified()) {
//    out.push_back(GhostFunctionCallExpr{// non const getter
//                                        GhostFunctionCallExpr::field,
//                                        {QualType(class_type, 0)},
//                                        member_type,
//                                        "_0." + name});
//  }
//  if (bigfive.test(BigFive::copy_assignable)) {
//    out.push_back(
//        GhostFunctionCallExpr{// non const getter
//                              GhostFunctionCallExpr::field,
//                              {QualType(class_type, 0), const_member_type},
//                              member_type,
//                              "_0." + name + " = _1"});
//  }

//  return out;
//}

std::unordered_map<AccessorType, Accessor>
generateAccessors(const Expr *expr, const FieldDecl *field,
                  const ASTContext &Context, const std::string &prefix,
                  const std::string &postfix) {
  std::unordered_map<AccessorType, Accessor> out;
  out.reserve(2);

  auto record = field->getParent();

  auto functions = GetFunctionCallExprs(expr, Context, prefix, postfix);
  if (functions.empty()) {
    return {};
  }

  using iterator = decltype(functions.begin());

  // remove not accessors
  auto first_invalid = std::remove_if(
      functions.begin(), functions.end(),
      [&](const GhostFunctionCallExpr &fn) -> bool {
        // getters have 1 arg(class) and setters have 2
        if (fn.arg_types.size() > 2)
          return true;

        auto class_t = fn.arg_types.front();

        if (class_t->isReferenceType() or class_t->isPointerType())
          class_t = class_t->getPointeeType();

        // if not auto, not a template and not a record of a field
        if (!isa<AutoType>(class_t) and !class_t->isTemplateTypeParmType() and
            (!class_t->isRecordType() or
             class_t.getTypePtr() != record->getTypeForDecl())) {
          return true;
        }

        // invalid getter
        if (fn.arg_types.size() == 1 and fn.return_type->isVoidType())
          return true;

        return false;
      });
  functions.erase(first_invalid, functions.end());
  if (functions.empty())
    return {};

  // sort by function types
  std::sort(
      functions.begin(), functions.end(),
      [](const GhostFunctionCallExpr &lhs, const GhostFunctionCallExpr &rhs) {
        return lhs.expr < rhs.expr;
      });

  unsigned groups_count_ = 1;
  auto next_group = functions.end();

  auto current = functions.begin();
  ++current;
  auto prev = functions.begin();
  while (current != functions.end()) {

    if (prev->expr != current->expr) {
      ++groups_count_;
      next_group = current;
    }

    prev = current;
    ++current;
  }

  auto handle_group = [&](iterator first, iterator last) -> bool {
    if (first == last)
      return true;

    if (first->arg_types.size() == 1) { // getter

      if (out.count(AccessorType::getter))
        return false;
      if (std::distance(first, last) > 2)
        return false;

      auto &getter = out[AccessorType::getter];

      getter.type = AccessorType::getter;
      getter.call_expr = first->expr;
      getter.original_expr = getFullyQualified(expr, Context);

      std::string signature;

      /**
       * sort by signatures
       * const return
       * non const return
       */
      std::sort(first, last,
                [](const GhostFunctionCallExpr &lhs,
                   [[maybe_unused]] const GhostFunctionCallExpr &rhs) -> bool {
                  auto l_type = lhs.arg_types.front();

                  return l_type.isConstQualified();
                });

      for (auto &fn : boost::make_iterator_range(first, last)) {
        signature = getFullyQualified(fn.return_type, Context) + '(';

        for (unsigned i = 0; i < fn.arg_types.size(); ++i) {
          signature += getFullyQualified(fn.arg_types[i], Context);
          if (i < fn.arg_types.size() - 1)
            signature += ", ";
        }
        signature += ')';

        getter.signatures.push_back(std::move(signature));
      }

    } else { // setter
      if (out.count(AccessorType::setter))
        return false; // error

      auto &setter = out[AccessorType::setter];

      setter.type = AccessorType::setter;
      setter.call_expr = first->expr;
      setter.original_expr = getFullyQualified(expr, Context);

      std::string signature;

      auto field_t = field->getType();
      if (field_t->isReferenceType())
        field_t = field_t->getPointeeType();

      field_t.removeLocalVolatile();
      field_t.removeLocalConst();
      field_t.removeLocalRestrict();

      auto cfield_t = field_t;
      cfield_t.addConst();

      /**
       * sort by signatures
       * set by type (field goes first, others go in whatever order)
       * set by value of field type
       * set by clref of field type
       * set by rref of field type
       * set by comparing string values
       */
      std::sort(first, last,
                [&](const GhostFunctionCallExpr &lhs,
                    const GhostFunctionCallExpr &rhs) -> bool {
                  auto l_type = lhs.arg_types.back();
                  auto r_type = rhs.arg_types.back();

                  QualType l_pointee = l_type->isReferenceType()
                                           ? l_type->getPointeeType()
                                           : l_type;
                  QualType r_pointee = r_type->isReferenceType()
                                           ? r_type->getPointeeType()
                                           : r_type;

                  l_pointee.removeLocalVolatile();
                  l_pointee.removeLocalRestrict();

                  r_pointee.removeLocalVolatile();
                  r_pointee.removeLocalRestrict();

                  // check pointee types
                  if (l_pointee.getTypePtr() == field_t.getTypePtr()) {
                    if (r_pointee.getTypePtr() != field_t.getTypePtr())
                      return true;
                  } else if (r_pointee.getTypePtr() == field_t.getTypePtr()) {
                    return false;
                  }

                  // if underlying types are different - sort by pointer (it
                  // doesn't matter anyway)
                  if (l_pointee.getTypePtr() != r_pointee.getTypePtr()) {
                    return l_pointee.getTypePtr() < r_pointee.getTypePtr();
                  }

                  // by value
                  if (l_type.getTypePtr() == field_t.getTypePtr()) {
                    if (r_type.getTypePtr() != field_t.getTypePtr())
                      return true;
                    else // well, both arguments are copy by value, so let's
                         // sort by first arg string (can't sort by pointer,
                         // because they can be the same underlying type but
                         // with different qualifieres)
                      return lhs.arg_types.front().getAsString() <
                             rhs.arg_types.front().getAsString();
                  } else if (r_type.getTypePtr() == field_t.getTypePtr())
                    return false;

                  // by clref
                  if (l_type->isLValueReferenceType() and
                      l_pointee.isConstQualified()) {
                    if (!r_type->isLValueReferenceType() or
                        !r_pointee.isConstQualified())
                      return true;
                    else // here we go again, look at previous if
                      return lhs.arg_types.front().getAsString() <
                             rhs.arg_types.front().getAsString();
                  } else if (r_type->isLValueReferenceType() and
                             r_pointee.isConstQualified())
                    return false;

                  // by rref or crref
                  if (l_type->isRValueReferenceType()) {
                    if (!r_type->isRValueReferenceType())
                      return true;
                    else // here we go again, look at previous if
                      return lhs.arg_types.front().getAsString() <
                             rhs.arg_types.front().getAsString();
                  } else if (r_type->isRValueReferenceType())
                    return false;

                  // in this place order doesn't matter, so let's just compare
                  // strings
                  int less = l_type.getAsString().compare(r_type.getAsString());
                  if (less == 0) { // here we go again, look at previous if
                    return lhs.arg_types.front().getAsString() <
                           rhs.arg_types.front().getAsString();
                  } else {
                    return less < 0;
                  }
                });

      for (auto &fn : boost::make_iterator_range(first, last)) {
        signature = getFullyQualified(fn.return_type, Context) + '(';

        for (unsigned i = 0; i < fn.arg_types.size(); ++i) {
          signature += getFullyQualified(fn.arg_types[i], Context);
          if (i < fn.arg_types.size() - 1)
            signature += ", ";
        }
        signature += ')';

        setter.signatures.push_back(std::move(signature));
      }
    }
    return true;
  };

  // handle first group
  if (!handle_group(functions.begin(), next_group))
    return {};

  // handle second group
  if (!handle_group(next_group, functions.end()))
    return {};

  return out;
}

Qualifiers getMethodQuals(const FunctionProtoType *ptr) {
  return ptr->getMethodQuals();
}

std::string to_string(GhostFunctionCallExpr::Type type) {

  static const std::vector<std::string> strings = {
      "variable", "field",    "method",     "function",
      "object",   "operator", "conversion", "constructor"};

  return strings[unsigned(type)];
}

std::string getFullyQualified(const Expr *expr, const ASTContext &Context) {
  static PrintingPolicy print_policy(Context.getLangOpts());
  print_policy.FullyQualifiedName = 1;
  print_policy.SuppressScope = 0;
  print_policy.SuppressUnwrittenScope = 0;
  print_policy.PrintCanonicalTypes = 1;

  struct Helper : public PrinterHelper {
    virtual bool handledStmt(Stmt *E, raw_ostream &OS) override {
      if (auto decl_ref = dyn_cast<DeclRefExpr>(E)) {
        OS << decl_ref->getFoundDecl()->getQualifiedNameAsString();
        if (decl_ref->hasExplicitTemplateArgs())
          printTemplateArgumentList(OS, decl_ref->template_arguments(),
                                    print_policy);
        return true;
      }

      return false;
    }

  } helper;

  std::string expr_string;
  llvm::raw_string_ostream stream(expr_string);
  expr->printPretty(stream, &helper, print_policy);
  stream.flush();
  return expr_string;
}

std::string getFullyQualified(QualType qtype, const ASTContext &Context) {
  static PrintingPolicy print_policy(Context.getLangOpts());
  print_policy.FullyQualifiedName = 1;
  print_policy.SuppressScope = 0;
  print_policy.SuppressUnwrittenScope = 0;
  print_policy.PrintCanonicalTypes = 1;

  return qtype.getAsString(print_policy);
}

GhostFunctionCallExpr generateOperatorCall(const FunctionDecl *decl,
                                           SourceLocation loc,
                                           const ASTContext &Context,
                                           const std::string &prefix,
                                           const std::string &postfix) {

  assert(decl->isOverloadedOperator() and
         "decl should be an overloaded operator");

  clang::DiagnosticsEngine &DE = Context.getDiagnostics();
  const unsigned not_supported_op =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Warning,
                         "Operator '%0' is not supported for reflection yet.");

  if (auto conversion = dyn_cast<CXXConversionDecl>(decl)) {
  }

  using Kind = OverloadedOperatorKind;

  auto method = dyn_cast<CXXMethodDecl>(decl);
  // if static - still treat as function
  method = method->isStatic() ? nullptr : method;

  std::vector<QualType> args;
  args.reserve(decl->getNumParams() + (method ? 1 : 0));
  QualType return_type = decl->getReturnType();

  if (method) {
    args.push_back(QualType(method->getParent()->getTypeForDecl(),
                            method->getMethodQualifiers().getAsOpaqueValue()));
  }
  for (const auto &param : decl->parameters()) {
    args.push_back(param->getType());
  }

  switch (decl->getOverloadedOperator()) {
  default:
    DE.Report(loc, not_supported_op)
        .AddString(getOperatorSpelling(decl->getOverloadedOperator()));
    break;
  // Arithmetic
  case Kind::OO_Equal:
    return GhostFunctionCallExpr{
        // const getter
        GhostFunctionCallExpr::op_call,
        {std::move(args)},
        std::move(return_type),
        method->getType(),
        prefix + "_0" + postfix + '=' + prefix + "_1" + postfix};
  }

  GhostFunctionCallExpr out{GhostFunctionCallExpr::op_call, std::move(args),
                            std::move(return_type), method->getType(), ""};

  auto op = decl->getOverloadedOperator();
  std::string spelling = getOperatorSpelling(op);
  switch (op) {

    // Binary operators:

    // Arithmetic
  case OO_Equal:

  case OO_Slash:
  case OO_Percent:
  case OO_PlusEqual:
  case OO_MinusEqual:
  case OO_StarEqual:
  case OO_SlashEqual:
  case OO_PercentEqual:
    // Comparison
  case OO_EqualEqual:
  case OO_ExclaimEqual:
  case OO_Greater:
  case OO_Less:
  case OO_GreaterEqual:
  case OO_LessEqual:
  case OO_Spaceship:
    // Logical
  case OO_AmpAmp:
  case OO_PipePipe:
    // Bitwise
  case OO_Pipe:
  case OO_Caret:
  case OO_LessLess:
  case OO_GreaterGreater:
  case OO_AmpEqual:
  case OO_PipeEqual:
  case OO_CaretEqual:
  case OO_LessLessEqual:
  case OO_GreaterGreaterEqual:
  // Member and pointer operators
  case OO_Arrow:
  case OO_ArrowStar:
  // Other
  case OO_Comma:
    out.expr = prefix + "_0" + postfix + spelling + prefix + "_1" + postfix;
    break;

  // Unary operators:
  case OO_Exclaim:
  case OO_Tilde:
    out.expr = spelling + prefix + "_0" + postfix;
    break;

  // Specials:
  case OO_Amp:
  case OO_Plus:
  case OO_Minus:
    if (out.arg_types.size() == 1) {
      out.expr = spelling + prefix + "_0" + postfix;
    } else {
      out.expr = prefix + "_0" + postfix + spelling + prefix + "_1" + postfix;
    }
    break;

  case OO_PlusPlus:
  case OO_MinusMinus:
    if (out.arg_types.size() == 1) {
      out.expr = spelling + prefix + "_0" + postfix;
    } else {
      out.expr = prefix + "_0" + postfix + spelling;
    }
    break;

  case OO_Subscript:
    out.expr = prefix + "_0" + postfix + '[' + prefix + "_1" + postfix + ']';
    break;
  case OO_Star: // special case
    if (out.arg_types.size() == 1)
      out.expr = '*' + prefix + "_0" + postfix;
    else
      out.expr = prefix + "_0" + postfix + '*' + prefix + "_1" + postfix;
    break;
  case OO_Call:
    out.expr.reserve(sizeof("_0()") + sizeof("_0,") * out.arg_types.size());
    out.expr = prefix + "_0" + postfix + '(';
    for (unsigned i = 0; i < out.arg_types.size(); ++i) {
      out.expr += prefix + "_" + std::to_string(i + 1) + postfix + "";
      if (i < out.arg_types.size() - 1)
        out.expr += ", ";
    }
    out.expr += ')';

    break;

  case OO_New:
  case OO_Array_New:
  case OO_Delete:
  case OO_Array_Delete:

  case OO_Conditional:
  case OO_Coawait:
  case OO_None:
  case NUM_OVERLOADED_OPERATORS:
    DE.Report(loc, not_supported_op).AddString(getOperatorSpelling(op));
    break;
  }
  return out;
}

BigFiveSet getBigFive(QualType type) {
  BigFiveSet out;

  static auto set_for_const = [](BigFiveSet &set) {
    set.set(BigFive::default_constructible);
    set.set(BigFive::copy_constructible);
    set.set(BigFive::move_constructible);
  };

  if (type->isReferenceType())
    type = type->getPointeeType();

  bool is_const = type.isConstQualified();

  if (type->isVoidType()) {
    out.reset();
  } else if (auto record =
                 dyn_cast_or_null<CXXRecordDecl>(type->getAsRecordDecl())) {

    for (auto method : record->methods()) {
      if (auto ctor = dyn_cast<CXXConstructorDecl>(method)) {
        if (ctor->isDefaultConstructor())
          out.set(BigFive::default_constructible);
        else if (ctor->isCopyConstructor())
          out.set(BigFive::copy_constructible);
        else if (ctor->isMoveConstructor())
          out.set(BigFive::move_constructible);
      } else if (method->isCopyAssignmentOperator() and
                 (!method->isConst() or !is_const)) {
        out.set(BigFive::copy_assignable);
      } else if (method->isMoveAssignmentOperator() and
                 (!method->isConst() or !is_const)) {
        out.set(BigFive::move_assignable);
      }

      if (out.all())
        break;
    }

  } else {
    if (!is_const)
      out.set();
    else
      set_for_const(out);
  }

  return out;
}

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXConversionDecl *conversion,
                     [[maybe_unused]] const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix) {
  return {GhostFunctionCallExpr{
      GhostFunctionCallExpr::conversion,
      {Context.getLValueReferenceType(
          QualType(conversion->getParent()->getTypeForDecl(),
                   conversion->getMethodQualifiers().getAsOpaqueValue()))},
      conversion->getConversionType(),
      conversion->getType(),
      "static_cast<" +
          getFullyQualified(conversion->getConversionType(), Context) + ">(" +
          prefix + "_0" + postfix + ')'}};
}

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXConstructorDecl *ctor,
                     [[maybe_unused]] const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix) {
  std::vector<QualType> args;
  args.reserve(ctor->param_size());
  std::string expr_str =
      getFullyQualified(QualType(ctor->getParent()->getTypeForDecl(), 0),
                        Context) +
      "(";
  expr_str.reserve(expr_str.size() + args.capacity() * 11);

  unsigned count = 0;
  for (const auto &arg : ctor->parameters()) {
    args.push_back(arg->getType());
    expr_str += prefix + "_" + std::to_string(count) + postfix + "";
    ++count;
    if (count != ctor->param_size())
      expr_str += ", ";
  }
  expr_str += ")";

  return {GhostFunctionCallExpr{
      GhostFunctionCallExpr::constructor, std::move(args),
      QualType(ctor->getParent()->getTypeForDecl(), 0), ctor->getType(), std::move(expr_str)}};
}

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const FunctionDecl *fn, const Expr *expr,
                     const ASTContext &Context);

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const CXXMethodDecl *method, const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix) {
  if (auto ctor = dyn_cast<CXXConstructorDecl>(method))
    return GetFunctionCallExprs(ctor, expr, Context, prefix, postfix);
  if (auto conversion = dyn_cast<CXXConversionDecl>(method))
    return GetFunctionCallExprs(conversion, expr, Context, prefix, postfix);
  if (method->isOverloadedOperator())
    return {generateOperatorCall(method, expr->getExprLoc(), Context, prefix,
                                 postfix)};
  if (method->isStatic())
    return GetFunctionCallExprs(cast<FunctionDecl>(method), expr, Context,
                                prefix, postfix);

  std::string template_args;
  if (auto unresolved = dyn_cast<UnresolvedLookupExpr>(expr)) {
    if (unresolved->hasExplicitTemplateArgs()) {
      PrintingPolicy print_policy(Context.getLangOpts());
      print_policy.FullyQualifiedName = 1;
      print_policy.SuppressScope = 0;
      print_policy.PrintCanonicalTypes = 1;
      llvm::raw_string_ostream os(template_args);
      printTemplateArgumentList(os, unresolved->template_arguments(),
                                print_policy);
    }
  }

  std::vector<QualType> args;
  args.reserve(method->param_size() + 1);
  std::string expr_str = prefix + "_0" + postfix + '.' +
                         method->getNameAsString() + template_args + "(";

  expr_str.reserve(expr_str.size() + args.capacity() * 11);

  args.push_back(Context.getLValueReferenceType(
      QualType(method->getParent()->getTypeForDecl(),
               method->getMethodQualifiers().getAsOpaqueValue())));

  unsigned count = 0;
  for (const auto &arg : method->parameters()) {
    args.push_back(arg->getType());
    expr_str += prefix + "_" + std::to_string(count + 1) + postfix + "";
    ++count;
    if (count != method->param_size())
      expr_str += ", ";
  }
  expr_str += ")";

  return {GhostFunctionCallExpr{GhostFunctionCallExpr::method, std::move(args),
                                method->getReturnType(), method->getType(), std::move(expr_str)}};
}

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const FunctionDecl *fn, const Expr *expr,
                     const ASTContext &Context, const std::string &prefix,
                     const std::string &postfix) {
  auto method = dyn_cast<CXXMethodDecl>(fn);
  if (method and !method->isStatic())
    return GetFunctionCallExprs(method, expr, Context, prefix, postfix);
  if (auto ctor = dyn_cast<CXXConstructorDecl>(fn))
    return GetFunctionCallExprs(ctor, expr, Context, prefix, postfix);
  if (auto conversion = dyn_cast<CXXConversionDecl>(fn))
    return GetFunctionCallExprs(conversion, expr, Context, prefix, postfix);

  if (fn->isOverloadedOperator())
    return {
        generateOperatorCall(fn, expr->getExprLoc(), Context, prefix, postfix)};

  std::string template_args;
  if (expr) {
    if (auto unresolved = dyn_cast<UnresolvedLookupExpr>(expr)) {
      if (unresolved->hasExplicitTemplateArgs()) {
        PrintingPolicy print_policy(Context.getLangOpts());
        print_policy.FullyQualifiedName = 1;
        print_policy.SuppressScope = 0;
        print_policy.PrintCanonicalTypes = 1;
        llvm::raw_string_ostream os(template_args);
        printTemplateArgumentList(os, unresolved->template_arguments(),
                                  print_policy);
      }
    }
  }

  std::vector<QualType> args;
  args.reserve(fn->param_size());
  std::string expr_str = fn->getQualifiedNameAsString() + template_args + "(";

  expr_str.reserve(expr_str.size() + args.capacity() * 11);

  unsigned count = 0;
  for (const auto &arg : fn->parameters()) {
    args.push_back(arg->getType());
    expr_str += prefix + "_" + std::to_string(count) + postfix + "";
    ++count;
    if (count != fn->param_size())
      expr_str += ", ";
  }
  expr_str += ")";

  return {GhostFunctionCallExpr{GhostFunctionCallExpr::function,
                                std::move(args), fn->getReturnType(),
                                fn->getType(),
                                std::move(expr_str)}};
}

std::vector<GhostFunctionCallExpr>
GetFunctionCallExprs(const Expr *expr, const ASTContext &Context,
                     const std::string &prefix, const std::string &postfix) {

  auto qtype = expr->getType();

  // If addrof - we can generate code without weird syntax (A.b instead of
  // A.*(&A::b))
  auto unary = dyn_cast<UnaryOperator>(expr);
  auto decl_ref = dyn_cast<DeclRefExpr>(expr);
  auto unresolved = dyn_cast<UnresolvedLookupExpr>(expr);
  if (unary or (decl_ref and !qtype->isRecordType()) or unresolved) {
    if ((unary and unary->getOpcode() == UnaryOperator::Opcode::UO_AddrOf)) {

      if (unary) {
        for (auto child : unary->children()) {
          decl_ref = dyn_cast<DeclRefExpr>(child);
          unresolved = dyn_cast<UnresolvedLookupExpr>(child);
        }
      }
    }

    if (decl_ref or unresolved) {
      std::vector<const FunctionDecl *> fn_decls;

      if (decl_ref) {
        auto decl = decl_ref->getFoundDecl();
        if (auto fn = dyn_cast<FunctionDecl>(decl)) {
          fn_decls.push_back(fn);
        }
      } else if (unresolved) {
        for (auto decl : unresolved->decls()) {
          if (auto fn_template = dyn_cast<FunctionTemplateDecl>(decl)) {
            fn_decls.push_back(fn_template->getTemplatedDecl());
          } else if (auto fn = dyn_cast<FunctionDecl>(decl)) {
            fn_decls.push_back(fn);
          }
        }
      }

      std::vector<GhostFunctionCallExpr> out;
      out.reserve(fn_decls.size());
      for (auto decl : fn_decls) {
        std::vector<GhostFunctionCallExpr> tmp;
        if (decl_ref)
          tmp = GetFunctionCallExprs(decl, decl_ref, Context, prefix, postfix);
        else if (unresolved)
          tmp =
              GetFunctionCallExprs(decl, unresolved, Context, prefix, postfix);
        out.insert(out.end(), std::make_move_iterator(tmp.begin()),
                   std::make_move_iterator(tmp.end()));
      }
      return out;
    }
  }

  if (qtype->isMemberFunctionPointerType()) {
    qtype = qtype.getUnqualifiedType().getDesugaredType(Context);

    auto member_pointer = dyn_cast<MemberPointerType>(qtype);

    auto function_proto = dyn_cast<FunctionProtoType>(
        qtype->getPointeeType().getDesugaredType(Context));

    auto class_type = Context.getLValueReferenceType(
        QualType(member_pointer->getClass()->getUnqualifiedDesugaredType(),
                 function_proto->getMethodQuals().getAsOpaqueValue()));

    std::vector<QualType> args{class_type};
    args.insert(args.end(), function_proto->getParamTypes().begin(),
                function_proto->getParamTypes().end());

    std::string expr_string = '(' + prefix + "_0" + postfix + ".*(" +
                              getFullyQualified(expr, Context) + "))(";

    for (unsigned i = 1; i < args.size(); ++i) {
      expr_string += prefix + "_" + std::to_string(i) + postfix + "";
      if (i != args.size() - 1)
        expr_string += ", ";
    }
    expr_string += ")";

    return std::vector<GhostFunctionCallExpr>{GhostFunctionCallExpr{
        GhostFunctionCallExpr::method, std::move(args),
        function_proto->getReturnType().getDesugaredType(Context),
        qtype->getPointeeType(),
        std::move(expr_string)}};
  }
  // Pointer to function
  else if (qtype->isFunctionPointerType() or
           isa<FunctionProtoType>(qtype.getTypePtr())) {
    qtype = qtype.getUnqualifiedType().getDesugaredType(Context);

    const FunctionProtoType *function_proto =
        isa<FunctionProtoType>(qtype.getTypePtr())
            ? dyn_cast<FunctionProtoType>(qtype)
            : dyn_cast<FunctionProtoType>(
                  qtype->getPointeeType().getDesugaredType(Context));

    std::string expr_str = "(" + getFullyQualified(expr, Context) + ")(";

    for (unsigned i = 0; i < function_proto->getNumParams(); ++i) {
      expr_str += prefix + "_" + std::to_string(i) + postfix + "";
      if (i != function_proto->getNumParams() - 1)
        expr_str += ", ";
    }
    expr_str += ")";

    return std::vector<GhostFunctionCallExpr>{
        {GhostFunctionCallExpr::function,
         std::vector<QualType>{function_proto->getParamTypes().begin(),
                               function_proto->getParamTypes().end()},
         function_proto->getReturnType().getDesugaredType(Context),
         qtype->getPointeeType(),
         std::move(expr_str)}};
  }
  // functor (maybe a lambda)
  else if (qtype->isRecordType()) {
    auto decl = qtype->getAsCXXRecordDecl();
    if (!decl)
      return {};

    if (decl->isLambda()) {
      return {};
    } else {
      std::vector<GhostFunctionCallExpr> out;
      for (auto method : decl->methods()) {
        if (method->isOverloadedOperator() and
            method->getOverloadedOperator() ==
                OverloadedOperatorKind::OO_Call) {
          std::vector<QualType> args;
          args.reserve(method->param_size());
          std::string args_str = "(";
          args_str.reserve(method->param_size() * 11);
          unsigned counter = 0;
          for (const auto &param : method->parameters()) {
            args.push_back(param->getType());
            args_str += prefix + "_" + std::to_string(counter) + postfix + "";
            ++counter;
            if (counter != method->param_size())
              args_str += ", ";
          }
          args_str += ")";

          out.push_back(GhostFunctionCallExpr{
              GhostFunctionCallExpr::object, std::move(args),
              method->getReturnType(),
              method->getType(),
              getFullyQualified(expr, Context) + std::move(args_str)});
        }
      }
      return out;
    }
  }

  return std::vector<GhostFunctionCallExpr>();
}

std::string to_string(AccessorType type) {
  static std::vector<std::string> names = {"getter", "setter"};
  return names[unsigned(type)];
}
