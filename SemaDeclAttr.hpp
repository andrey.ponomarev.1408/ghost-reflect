/*
 * Insert switch statements bellow to the end of switch
 * statement in ProcessDeclAttribute inside SemaDeclAttr.cpp
 * 
 */
// Ghost Reflect Attributes
case ParsedAttr::AT_GhostReflect:
handleGhostReflect(S, D, AL);
break;
case ParsedAttr::AT_GhostFlag:
handleGhostFlag(S, D, AL);
break;    
  case ParsedAttr::AT_GhostProperty:
handleGhostProperty(S, D, AL);
break;
case ParsedAttr::AT_GhostDeclareAction:
handleGhostActionDeclaration(S, D, AL);
break;
case ParsedAttr::AT_GhostAction:
handleGhostAction(S, D, AL);
break;


/**
 * Insert functions bellow into SemaDeclAttr.cpp (before ProcessDeclAttribute)
 */
//===----------------------------------------------------------------------===//
// Ghost Reflect attribute handles
//===----------------------------------------------------------------------===//
bool getGhostFlagVal(const ASTContext &Context, const ParsedAttr &AL,
                     bool &value) {
  
  if (AL.getNumArgs() >= 1) {
    Expr *E = AL.getArgAsExpr(0);
    
    if (E->EvaluateAsBooleanCondition(value, Context))
      return false;
  }
  
  return true;
}

void handleGhostReflect(Sema &S, Decl *D, const ParsedAttr &AL) {
  
  bool value = true;
  
  auto &DE = S.Context.getDiagnostics();
  unsigned invalid_argument =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid argument '%0' of reflect attribute");
  
  if (AL.getNumArgs() == 1) {
    Expr *E = AL.getArgAsExpr(0);
    
    if (!E or !E->EvaluateAsBooleanCondition(value, S.Context)) {
      DE.Report(AL.getLoc(), invalid_argument)
          .AddTaggedVal(0, DiagnosticsEngine::ArgumentKind::ak_sint);
      return;
    }
  } else if (AL.getNumArgs() > 1) {
    DE.Report(AL.getLoc(), invalid_argument)
        .AddTaggedVal(1, DiagnosticsEngine::ArgumentKind::ak_sint);
    return;
  }
  
  auto attr = new (S.Context) GhostFlagAttr(
      AL.getRange(), S.Context, "reflect", value, AL.getAttributeSpellingListIndex());
  
  D->addAttr(attr);
}

void handleGhostFlag(Sema &S, Decl *D, const ParsedAttr & AL) {
  
  StringRef name;
  bool value = true;
  
  auto &DE = S.Context.getDiagnostics();
  unsigned invalid_argument =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid argument '%0' of flag attribute, should "
                         "%select{be a string or identifier|be "
                         "a boolean|have from 1 to 2 args}.");
  
  if (AL.isArgIdent(0)) {
    name = AL.getArgAsIdent(0)->Ident->getName();
  } else {
    auto str = dyn_cast_or_null<StringLiteral>(AL.getArgAsExpr(0));
    if (!str) {
      auto report = DE.Report(AL.getLoc(), invalid_argument);
      report.AddTaggedVal(0, DiagnosticsEngine::ArgumentKind::ak_sint);
      report.AddTaggedVal(0, DiagnosticsEngine::ArgumentKind::ak_sint);
      return;
    }
    name = str->getString();
  }
  
  if (AL.getNumArgs() > 2) {
    auto report = DE.Report(AL.getLoc(), invalid_argument);
    report.AddTaggedVal(2, DiagnosticsEngine::ArgumentKind::ak_sint);
    report.AddTaggedVal(2, DiagnosticsEngine::ArgumentKind::ak_sint);
    return;
  }
  
  if (AL.getNumArgs() == 2) {
    Expr *E = AL.getArgAsExpr(1);
    
    if (!E or !E->EvaluateAsBooleanCondition(value, S.Context)) {
      auto report = DE.Report(AL.getLoc(), invalid_argument);
      report.AddTaggedVal(1, DiagnosticsEngine::ArgumentKind::ak_sint);
      report.AddTaggedVal(1, DiagnosticsEngine::ArgumentKind::ak_sint);
      return;
    }
  }
  
  
  auto attr = new (S.Context) GhostFlagAttr(
      AL.getRange(), S.Context, name, value, AL.getAttributeSpellingListIndex());
  
  D->addAttr(attr);
}

void handleGhostProperty(Sema &S, Decl *D, const ParsedAttr &AL) {
  
  std::vector<Expr *> exprs;
  exprs.reserve(AL.getNumArgs());
  
  clang::DiagnosticsEngine &DE = S.Context.getDiagnostics();
  static const unsigned invalid_subject =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid subject of property attribute. Should be a "
                         "field or global variable declaration");
  
  
  static const unsigned invalid_arg_type =
      DE.getCustomDiagID(clang::DiagnosticsEngine::Error,
                         "Invalid argument of property attribute. Should be an accessor.");
  
  auto field = dyn_cast<FieldDecl>(D);
  auto var = dyn_cast<VarDecl>(D);
  
  
  
  if (!field and !var) {
    DE.Report(AL.getLoc(), invalid_subject);
    return;
  }
  
  
  for (unsigned i = 0; i < AL.getNumArgs(); ++i) {
    if (!AL.isArgExpr(i)) {
      DE.Report(AL.getArgAsIdent(i)->Loc, invalid_arg_type);
      return;
    }
    exprs.push_back(AL.getArgAsExpr(i));
  }
  
  auto attr = new (S.Context) GhostPropertyAttr(
      AL.getRange(), S.Context, exprs.data(), unsigned(exprs.size()),
      AL.getAttributeSpellingListIndex());
  
  D->addAttr(attr);
}


void handleGhostActionDeclaration(Sema &S, Decl *D, const ParsedAttr &AL) {
  auto &Context = S.Context;
  auto &DE = Context.getDiagnostics();
  
  unsigned common_error = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "'declare_action' attribute argument %0 should be a %select{name of "
      "action|boolean value for allowing raw expressions|name of helper}1");
  
  if (AL.getNumArgs() < 3) {
    unsigned diag_id = DE.getCustomDiagID(
        DiagnosticsEngine::Error,
        "'declare_action' attribute should have at least a name, boolean value "
        "for allowing raw expressions and one helper name");
    DE.Report(AL.getLoc(), diag_id);
  }
  
  
  if (!isa<EmptyDecl>(D)) {
    unsigned diag_id = DE.getCustomDiagID(
        DiagnosticsEngine::Error,
        "'declare_action' attribute should be applied to empty declarations.");
    DE.Report(AL.getLoc(), diag_id);
    return;
  }
  
  StringRef name;
  if (auto name_literal = dyn_cast_or_null<StringLiteral>(AL.getArgAsExpr(0))) {
    name = name_literal->getString();
  } else if (auto ident = AL.getArgAsIdent(0)) {
    name = ident->Ident->getName();
  } else {
    DE.Report(AL.getLoc(), common_error) << 0 << 0;
    return;
  }
  
  bool allow_exprs;
  auto bool_expr = AL.getArgAsExpr(1);
  if (bool_expr and bool_expr->isKnownToHaveBooleanValue()) {
    bool_expr->EvaluateAsBooleanCondition(allow_exprs, Context);
  } else {
    DE.Report(AL.getLoc(), common_error) << 1 << 1;
    return;
  }
  
  std::vector<StringRef> helper_names;
  helper_names.reserve(AL.getNumArgs() - 2);
  
  for (unsigned i = 2; i < AL.getNumArgs(); ++i) {
    if (auto string_literal =
            dyn_cast_or_null<StringLiteral>(AL.getArgAsExpr(i))) {
      helper_names.push_back(string_literal->getString());
    } else {
      DE.Report(AL.getLoc(), common_error) << i << 2;
      return;
    }
  }
  
  auto attr = new (S.Context) GhostDeclareActionAttr(
      AL.getRange(), S.Context, name, allow_exprs, helper_names.data(),
      unsigned(helper_names.size()), AL.getAttributeSpellingListIndex());
  D->addAttr(attr);
}


void handleGhostAction(Sema &S, Decl *D, const ParsedAttr &AL) {
  
  auto &Context = S.Context;
  auto &DE = Context.getDiagnostics();
  
  unsigned common_error = DE.getCustomDiagID(
      DiagnosticsEngine::Error,
      "'action' attribute argument %0 should be a %select{name of "
      "action|helper expression}1");
  
  if (AL.getNumArgs() < 2) {
    unsigned diag_id = DE.getCustomDiagID(
        DiagnosticsEngine::Error, "'declare_action' attribute should have at "
                                  "least a name and one helper expression");
    DE.Report(AL.getLoc(), diag_id);
  }
  
  StringRef name;
  if (auto name_literal = dyn_cast_or_null<StringLiteral>(AL.getArgAsExpr(0))) {
    name = name_literal->getString();
  } else if (auto ident = AL.getArgAsIdent(0)) {
    name = ident->Ident->getName();
  } else {
    DE.Report(AL.getLoc(), common_error) << 0 << 0;
    return;
  }
  
  std::vector<Expr *> exprs;
  exprs.reserve(AL.getNumArgs() - 1);
  for (unsigned i = 1; i < AL.getNumArgs(); ++i) {
    if (auto expr = AL.getArgAsExpr(i))
      exprs.push_back(expr);
    else {
      DE.Report(AL.getLoc(), common_error) << i << 1;
      return;
    }
  }
  
  auto attr = new (S.Context) GhostActionAttr(
      AL.getRange(), S.Context, name, exprs.data(),
      unsigned(exprs.size()), AL.getAttributeSpellingListIndex());
  D->addAttr(attr);
  
} 
