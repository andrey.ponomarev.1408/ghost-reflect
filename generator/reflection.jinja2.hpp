##-
##-
##-
{%-macro declare_function(signature, name, args) %}
{%- endmacro %}
##-
##-
##-
{%- macro print_bool(x) %}
{$ "true" if x else "false" $}
{%- endmacro %}
##-
##-
##-
##-
##-
##-
{%- macro print_access(access) -%}
refl::Access::{$ access[0]|upper $}{$access[1:] $}
{%- endmacro %}
##-
##-
##-
{%- macro print_kind(kind) -%}
refl::Kind::{$ kind[0]|upper $}{$kind[1:] $}
{%- endmacro %}
##-
##-
##-
{%- macro filter_float(x) -%}
{% if x[-1] == "F" %}{$ x[:-1] $}f{% else %}{$ x $}{% endif %}
{%- endmacro %}
##-
##-
##-
{%- macro declare_helpers(type_name, helpers) -%}
{%- for helper_name in helpers %}
{%- set helper = helpers[helper_name] %}

template<{$type_name$}, {$ loop.index0 $}>
struct Helper {
  static constexpr refl::HelperStorage      storage = refl::HelperStorage::{$ helper.storage $};
  static constexpr refl::HelperStorageType  storage_type = refl::HelperStorageType::{$ helper.storage_type $};  

  static constexpr auto arguments = refl::{$ 'make_tuple' if helper.storage == 'array' else 'make_map' $} (
{% for argument in helper.arguments %}
{% if argument.call_exprs%}
        refl::make_tuple(
{% for call_expr in argument.call_exprs %}
          refl::cast_fn<{$ call_expr.signature $}>({$argument.raw_expr$}){$ "," if not loop.last $}  
{% endfor %}
        ){$ "," if not loop.last $}
{% else %}
        {$ filter_float(argument.raw_expr) $}{$ "," if not loop.last $}  
{% endif %}
{% endfor %}
  );
};
{% endfor %}
{%- endmacro %}
##-
##-
##-
{%- macro declare_bases(type_name, bases) -%}
{%- for base in bases %}

template<>
struct Base<{$ type_name $}, {$ loop.index0 $}> {
  using value_type = {$ base.name $};
  static constexpr auto type = refl::type<value_type>();
  static constexpr auto is_virtual = {$ print_bool(base.is_virtual) $};
  static constexpr auto access = {$ print_access(base.access) $};
};
{%- endfor %}
{%- endmacro %}
##-
##-
##-
{%- macro declare_all_bases(type_name, all_bases) -%}
{%- for base in all_bases %}

template<>
struct AllBase<{$ type_name $}, {$ loop.index0 $}> {
  using value_type = {$ base $};
  static constexpr auto type = refl::type<value_type>();
};
{%- endfor %}
{%- endmacro %}
##-
##-
##-
{%- macro declare_fields(type_name, fields) -%}
{%- for field in fields %}
{%- set field_type_name = "Field<" ~ type_name ~ ", " ~ loop.index0 ~ ">" %}

// Helpers for field {$ type_name $}::{$ field.name $}
{$ declare_helpers(field_type_name, field.helpers) $}

template<>
struct {$field_type_name$} {
  using value_type = {$ field.type $};
  static constexpr auto type = refl::type<value_type>();

  static constexpr auto name = {$ field.name $};

  static constexpr auto flags = refl::make_map(
    {$define_flags(field.flags) | indent(4, True)$}
  );
  static constexpr auto helpers = refl::make_map(
    {$ define_helpers(field.helpers) | indent(4, True) $} 
  );

  static constexpr auto access = {$ print_access(field.access) $};

  static constexpr bool has_getter = {$ print_bool(not not field.getter) $};
  static constexpr bool has_setter = {$ print_bool(not not field.setter) $};

  {% if field.access == "public" %}
  static inline const value_type& get(const {$ full_name $}& obj) {
    return obj.{$field.name$};
  }
  static inline value_type& get({$ full_name $}& obj) {
    return obj.{$field.name$};
  }
  {%- elif field.getter %}
  {%- for signature in field.getter.signatures %}
  {%- set get = ('(.+)\((.+)\)') | regex_sub('static inline \\1 get (\\2 obj)', signature) %}

  {$ get $} {
    return {$ field.getter.call_expr | render_template("obj") $};
  }
  {%- endfor %}
  {%- endif %}
  
  {% if field.access == "public"%}
  template<typename T>
  static inline void set({$ full_name $}& obj, T&& val) {
    obj.{$ field.name $} = std::forward<T>(val);
  }
  {%- elif field.setter %}
  {%- for signature in field.setter.signatures %}
  {%- set set = ('(.+)\((.+),(.+)\)') | regex_sub('static inline \\1 set (\\2 obj, \\3 val)', signature) %}

  {$ set $} {
    return {$ field.setter.call_expr | render_template("obj", "val") $};
  }
  {%- endfor %}
  {%- endif %}

};


{%- endfor %}
{%- endmacro %}
##-
##-
##-
{%- macro declare_methods(type_name, methods) -%}

{%- endmacro %}
##-
##-
##-
{%- macro define_flags(flags) -%}
{% for flag in flags%}
{refl::Flag::{$flag$}, {$"true" if flags[flag] else "false" $}}{$ "," if not loop.last $}
{% endfor %}
##-
##-
##-
{%- endmacro %}
{%- macro define_helpers(type_name, helpers) -%}
{% for helper_name in helpers %}
{ refl::Helper::{$ helper_name $}, refl::description::Helper<{$ type_name $}, {$ loop.index0 $}> }{$ "," if not loop.last $}
{% endfor %}
{%- endmacro %}
##-
##-
##-
{%- macro define_bases(type_name, bases) -%}

{%- endmacro %}
##-
##-
##-
{%- macro define_all_bases(type_name, all_bases) -%}

{%- endmacro %}
##-
##-
##-
{%- macro define_fields(type_name, fields) -%}

{%- endmacro %}
##-
##-
##-
{%- macro define_methods(type_name, methods) -%}

{%- endmacro %}
##-
##-
##-
{% if data.access == "none" or data.access == "public" %}
{% set full_name = prefix + data.name%}

#ifndef GHOST_REFLECT
//////////////////////////////////////////////
// Reflecting {$ full_name $}
//////////////////////////////////////////////

namespace refl {

  namespace description {
  // Helpers
    {$ declare_helpers(full_name, data.helpers) | indent(2, True) $}
    
    {%- if data_type == "record" %}


  // Bases
    {$ declare_bases(full_name, data.bases) | indent(2, True) $}
  
  // AllBases
    {$ declare_all_bases(full_name, data.all_bases) | indent(2, True) $}
  
  // Fields
    {$ declare_fields(full_name, data.fields) | indent(2, True) $}
  
  // Methods
    {$ declare_methods(full_name, data.methods)  | indent(2, True)$}
    {%- elif data_type == "enum" %}

    {%- endif %}
##-
  }

  template<>
  struct type<{$ full_name $}> {
    using value_type = {$ full_name $};
    {%- if data_type == "enum" %}
    using underlying_type = std::underlying_type<{$ full_name $}>;
    {%- endif %}
    static constexpr bool is_reflected = true;
    
    static constexpr auto name = "{$ data.name $}";
    static constexpr auto full_name = "{$ full_name $}";

    static constexpr auto flags = refl::make_map(
{$ define_flags(data.flags) | indent(6,True) $}
    );
    static constexpr auto helpers = refl::make_map(
{$ define_helpers(full_name, data.helpers) | indent(6,True) $}
    );

    static constexpr auto access = {$ print_access(data.access) $};
    static constexpr auto kind = {$ print_kind(data_type) $};
    {%- if data_type == "record" %}
    
    
    static constexpr auto bases = refl::make_tuple(
{$ define_bases(full_name, data.bases) | indent(6,True) $}
    );
    static constexpr auto all_bases = refl::make_tuple(    
{$ define_all_bases(full_name, data.all_bases) | indent(6,True) $}
    );
    
    
    static constexpr auto fields = refl::make_tuple(
{$ define_fields(full_name, data.fields) | indent(6,True) $}
    );
    static constexpr auto methods = refl::make_tuple(
{$ define_methods(full_name, data.methods) | indent(6,True) $}
    );
    {%- elif  data_type == "enum" %}

    {%- endif %}
  
  };
}
#endif
{%- endif %}