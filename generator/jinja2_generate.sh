SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

python3.7 $SCRIPTPATH/jinja2_generate.py "$@"
