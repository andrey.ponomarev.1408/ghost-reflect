import re
import json
import subprocess
import os
import sys, argparse
from datetime import date

from jinja2 import Environment, FileSystemLoader
from subprocess import Popen, PIPE, STDOUT

import re

jinja_enviroment = Environment()

def regex_sub(reg, rep, string):

  string = re.sub(reg, rep, string)
  return string

def render_template(template_str, *args):
  global jinja_enviroment
  dictionary = {}
  for index, arg in enumerate(args, start=0):
    dictionary['_'+str(index)]=str(arg)

  template = jinja_enviroment.from_string(template_str)
  return template.render(dictionary)

def contains(container, data):
  return data in container

def Traverse(json_obj: dict, template, prefix=""):
  for field, value in json_obj.items():        
    if field == "namespaces":
      for ns in value:
        name = ns["name"]
        Traverse(ns, template, prefix= prefix+name+"::" if name else prefix)
    elif field == "records":
      for record in value:
        result = template.render(prefix=prefix, data_type="record", data=record, env=os.environ, date=date.today().strftime("%B %d, %Y"))
        print(result)
        Traverse(record, template)
    elif field == "enums":
      for enum_decl in value:
        result = template.render(prefix=prefix, data_type="enum", data=enum_decl, env=os.environ, date=date.today().strftime("%B %d, %Y"))


def ProccessFile(filename, search_path, template_file, mode, var_start, var_end):
  global jinja_enviroment
  templateLoader = FileSystemLoader(searchpath=search_path, followlinks=True)
  templateEnv = Environment(loader=templateLoader,            \
                            variable_start_string=var_start,  \
                            variable_end_string=var_end,      \
                            line_comment_prefix="##",         \
                            trim_blocks=True,                 \
                            lstrip_blocks=True)

  templateEnv.filters['render_template'] = render_template
  templateEnv.filters['regex_search'] = re.search
  templateEnv.filters['regex_sub'] = regex_sub

  templateEnv.filters['contains'] = contains
  template = templateEnv.get_template(template_file)
  
  jinja_enviroment = templateEnv

  if mode == 'json':
    with open(filename, 'r') as f:
      data = json.load(f)
    if data:
      Traverse(data, template)
  elif mode == 'txt':
    with open(filename) as f_in:
      lines = (line.rstrip() for line in f_in) 
      lines = list(line for line in lines if line) # Non-blank lines in a list
      result = template.render(data_type="line_list", data=lines, env=os.environ, date=date.today().strftime("%B %d, %Y"))
      print(result)


  # print(template.render(data=data, env=os.environ, date=date.today().strftime("%B %d, %Y")))


# Main
parser = argparse.ArgumentParser(description='Generates text files from json files and jinja2 template.')
parser.add_argument('template', metavar='template', type=str,
                    help='Jinja2 template file')
parser.add_argument('files', metavar='files', nargs='+',
                    help='List of files(json or text)')
parser.add_argument('--variable-start', dest='var_start', type=str, default='{$', help='Defines jinja2 variable start delimiter')
parser.add_argument('--variable-end', dest='var_end', type=str, default='$}', help='Defines jinja2 variable end delimiter')
parser.add_argument('--mode', dest='mode', type=str, default='json', help='Defines mode (txt or json)')

args = parser.parse_args()
if os.path.exists(args.template):
  abs_path = os.path.abspath(args.template)
  directory = os.path.dirname(abs_path)
  template = os.path.basename(abs_path)
  for filename in args.files:
    ProccessFile(filename, directory, template, args.mode, args.var_start, args.var_end)
else:
  raise("failed to open template file")


