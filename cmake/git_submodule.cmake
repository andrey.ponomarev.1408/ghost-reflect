

macro(add_git_submodule PACKAGE_NAME)

  
  find_package(Git QUIET)
  if(GIT_FOUND AND EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git")
  # Update submodules as needed
      option(GIT_SUBMODULE "Check submodules during build" ON)
      if(GIT_SUBMODULE)
          message(STATUS "Submodule update")
          execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                          RESULT_VARIABLE GIT_SUBMOD_RESULT)
          if(NOT GIT_SUBMOD_RESULT EQUAL "0")
              message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
          endif()
      endif()
  endif()
  
  if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/external/${PACKAGE_NAME}")
      message(FATAL_ERROR "The submodule were not downloaded!
        GIT_SUBMODULE was turned off or failed.
        Please update submodules and try again.
        (${CMAKE_CURRENT_SOURCE_DIR}/external/${PACKAGE_NAME})")
  endif()
endmacro()



macro(add_package_subdir PACKAGE_NAME)
  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/external/${PACKAGE_NAME})
endmacro()

macro(add_interface_lib PACKAGE_NAME)
  add_library(${PACKAGE_NAME} INTERFACE)
  if (EXISTS ${PROJECT_SOURCE_DIR}/external/${PACKAGE_NAME}/include)
    target_include_directories(${PACKAGE_NAME} INTERFACE ${PROJECT_SOURCE_DIR}/external/${PACKAGE_NAME}/include)
  else()
    target_include_directories(${PACKAGE_NAME} INTERFACE ${PROJECT_SOURCE_DIR}/external/${PACKAGE_NAME})
  endif()
endmacro()
