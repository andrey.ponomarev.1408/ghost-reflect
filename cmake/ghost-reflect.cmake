#set(GHOST_REFLECT_PARSER_EXE CACHE FILEPATH "")
#set(GHOST_REFLECT_GENERATOR_EXE CACHE FILEPATH "")


macro(ghost_parse)
  
  if (NOT GHOST_REFLECT_PARSER_EXE)
    message(FATAL_ERROR "GHOST_REFLECT_PARSER_EXE is not specified")
  endif()
  
  cmake_parse_arguments(
      ARG # prefix of output variables
      "" # list of names of the boolean arguments (only defined ones will be true)
      "OUTPUT;" # list of names of mono-valued arguments
      "INPUT;WORKING_DIRECTORY" # list of names of multi-valued arguments (output variables are lists)
      ${ARGN} # arguments of the function to parse, here we take the all original ones
  )
  # note: if it remains unparsed arguments, here, they can be found in variable PARSED_ARGS_UNPARSED_ARGUMENTS
  if(NOT ARG_INPUT)
      message(FATAL_ERROR "You must provide at least one input file")
  endif()
  if (NOT ARG_OUTPUT)
    message(FATAL_ERROR "You must provide output file")
  endif()
  if (NOT ARG_WORKING_DIRECTORY)
    set(ARG_WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
  endif()

  add_custom_command(
    OUTPUT ${ARG_OUTPUT}
    COMMAND ${GHOST_REFLECT_PARSER_EXE} ${ARG_INPUT} -o ${ARG_OUTPUT}
    DEPENDS ${ARG_INPUT}
    COMMENT "Generating json file ${ARG_OUTPUT}"
    WORKING_DIRECTORY ${ARG_WORKING_DIRECTORY})
  
  set(ARG_INPUT)
  set(ARG_OUTPUT)
  set(ARG_WORKING_DIRECTORY)
  
endmacro()


function(ghost_generate)
  if (NOT GHOST_REFLECT_GENERATOR_EXE)
    message(FATAL_ERROR "GHOST_REFLECT_GENERATOR_EXE is not specified")
  endif()

  set(BOOLEAN_ARGS )
  set(MONOVALUED_ARGS INPUT_COMMON TEMPLATE OUTPUT WORKING_DIR)
  set(MULTIVALUED_ARGS INPUT)
  
  cmake_parse_arguments(
      ARG # prefix of output variables
      ${BOOLEAN_ARGS} # list of names of the boolean arguments (only defined ones will be true)
      ${MONOVALUED_ARGS} # list of names of mono-valued arguments
      ${MULTIVALUED_ARGS} # list of names of multi-valued arguments (output variables are lists)
      ${ARGN} # arguments of the function to parse, here we take the all original ones
  )
  if (NOT ARG_INPUT OR NOT ARG_TEMPLATE OR NOT ARG_OUTPUT)
    message(FATAL_ERROR "You should provide INPUT, TEMPLATE, OUTPUT and optionally INPUT_COMMON")
  endif()

  if (NOT ARG_INPUT_COMMON)
    add_custom_command(
      OUTPUT ${ARG_OUTPUT}
      COMMAND GHOST_REFLECT_EXE ${ARG_TEMPLATE} ${ARG_INPUT} > ${ARG_OUTPUT})
  else()
    message(FATAL_ERROR "Common files are not supported yet")  
  endif()
  
endfunction()
