This is a code generation tool. 
It consists of 2 parts: 
 * parser(for generating json representation of AST)
 * generator(for generating code from JINJA2 template and json files, created by parser)

This tool is currently in development, documentation and better description is going be provided later.
For compile instructions see CompileInstructions.txt

Windows binaries can be downloaded from the sourceforge:
https://sourceforge.net/projects/ghost-reflect/files/