/*!
 * \file      serialize_interface.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include <type_traits>
#include <string>

#include <rapidjson/document.h>

#include <vector>

namespace JsonSerialize {

using namespace rapidjson;

namespace description {
template <typename T> struct SerializerImpl;
template <typename T> struct DeserializerImpl;
}

template <typename T> struct Serializer;
template <typename T> struct Deserializer;



namespace description {
template <typename T> struct SerializerImpl {
  static_assert(
      std::is_integral_v<T> or std::is_floating_point_v<T>,
      "Default JsonSerializerImpl can handle only strings, vectors, integer "
      "and floating point types");

  void operator()(const T &val, Value& json_val, [[maybe_unused]] Document::AllocatorType &alloc) {
    json_val = Value(val);
  }
};

template <typename T> struct DeserializerImpl {
  static_assert(
      std::is_integral_v<T> or std::is_floating_point_v<T>,
      "Default JsonSerializerImpl can handle only strings, vectors, integer "
      "and floating point types");

  void operator()(T& val, const Value &json_val) { val = json_val.Get<T>(); }
};

template <> struct SerializerImpl<std::string> {
  void operator()(const std::string &str, Value& json_val, Document::AllocatorType &alloc) {
    json_val = Value(str.c_str(), alloc);
  }
};

template <> struct DeserializerImpl<std::string> {
  void operator()(std::string& val, const Value &json_val) {
    val = json_val.GetString();
  }
};

template <typename T, typename Allocator>
struct SerializerImpl<std::vector<T, Allocator>> {
  void operator()(const std::vector<T, Allocator> &vec,
                  Value& json_val,
                   Document::AllocatorType &alloc) {
    json_val.SetArray();
    for (const auto &val : vec) {
      json_val.PushBack(Serializer<T>()(val, alloc), alloc);
    }
  }
};

template <typename T, typename Allocator>
struct DeserializerImpl<std::vector<T, Allocator>> {
  void operator()(std::vector<T, Allocator>& val, const Value &json_val) {
    auto array = json_val.GetArray();
    val.reserve(json_val.Size());
    for (unsigned i = 0; i < json_val.Size(); ++i) {
      val.emplace_back(Deserializer<T>()(array[i]));
    }
  }
};

} // namespace description

template <typename T> struct Serializer {

  Value operator()(const T &val, Document::AllocatorType &alloc) {
    Value out;
    description::SerializerImpl<T>()(val, out, alloc);
    return out;
  }
};

template <typename T> struct Deserializer {

  T operator()(const Value &val) {
    T out;
    description::DeserializerImpl<T>()(out, val);
    return out;
  }
};

template <typename T>
Value Serialize(const T &val, Document::AllocatorType &alloc) {
  return Serializer<std::decay_t<T>>()(val, alloc);
}

template <typename T> std::decay_t<T> Deserialize(const Value &json_val) {
  return Deserializer<std::decay_t<T>>()(json_val);
}

} // namespace JsonSerilize

