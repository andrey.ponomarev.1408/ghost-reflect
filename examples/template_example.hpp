
template<typename T>
struct templated_struct {
  T template_definition;
};

template<>
struct templated_struct<int> {
  int template_specialization;
};

template<typename T>
struct templated_struct<templated_struct<T>> {
  templated_struct<T> template_partial_specialization;
  template<typename U>
  struct nested_template {
    
  };
};


template<typename T>
void deduceable_fn(T&&) {
  
}

template<typename T>
T not_deduceable_fn(T&&) {
  return T();
}

template<typename T, typename U>
U partially_deduceable_fn(T&&) {
  
}

