/* possible types:
  "functors"
  "raw_exprs" 
  "mix"
  "array_functors"
  "array_raw_exprs" 
  "array_mix"
*/

void foo();
void bar(int i);

static constexpr int baz = 0;



// Helper that has only functors: fn_foo, fn_bar
[[ declare_helper("fn_helper", "map", "functors", "fn_foo", "fn_bar")]];
// Helper that has raw expressions(could be a literal, variable, actual expression): some_int, some_string_literal, random_expr
[[ declare_helper("raw_helper", "map", "raw_exprs", "some_int", "some_string_literal", "random_expr")]]; 
// Helper that can have both callable and raw expressions: some_int, some_fn
[[ declare_helper("mix_helper", "map", "mix", "some_int", "some_fn")]];

// Helper that has array of functors
[[ declare_helper("array_fn_helper", "array", "functors")]];
// Helper that has array of raw expressions
[[ declare_helper("array_raw_helper", "array", "raw_exprs")]]; 
// Helper that has array of both functors and raw expressions
[[ declare_helper("array_mix_helper", "array", "mix")]];


namespace [[reflect]] ns {





/*
 *  Will generate this json(fn_helper is array in case of multiple definitions):
    "helpers" : {
      "fn_helper" : [
        {
          "fn_foo" : {
            "raw_expr" : "foo"
            "is_callable" : true,
            "call_exprs" : ["foo()"],
            "call_signatures" : [ "void()" ]
          },
          "fn_bar" : {
            "raw_expr" : "bar",
            "is_callable" : true,
            "call_exprs" : ["bar({{_0}})"],
            "call_signatures" : ["void(int)"]
          }
        }
      ]
    }
 * 
 */
struct [[helper("fn_helper", foo, bar)]] fn_struct {};







/*
 *  Will generate this json(foo is_callable false, because the mode is raw_expr and it doesn't support call resolution):
    "helpers" : {
      "raw_helper" : [ 
        {
          "some_int" : {
            "raw_expr" : "baz"
            "is_callable" : false
          },
          "some_string_literal" : {
            "raw_expr" : "\"my string literal\"",
            "is_callable" : false
          },
          "random_expr" : {
            "raw_expr" : "foo",
            "is_callable" : false
          }
        }
      ]
    }
 * 
 */
struct [[helper("raw_helper", baz, "my string literal", foo)]] raw_expr_struct {};








/*
 *  Will generate this json(fn_helper is array in case of multiple definitions):
    "helpers" : {
      "mix_helper" : [
        {
          "some_int" : {
            "raw_expr" : "baz"
            "is_callable" : false,
          },
          "some_fn" : {
            "raw_expr" : "bar",
            "is_callable" : true,
            "call_exprs" : ["bar({{_0}})"],
            "call_signatures" : ["void(int)"]
          }
        }
      ]
    }
 * 
 */
struct [[helper("mix_helper", baz, bar)]] mix_struct {};


/*
 *  Will generate this json(fn_helper is array in case of multiple definitions):
    "helpers" : {
      "array_fn_helper" : [
        [
          {
            "raw_expr" : "foo"
            "is_callable" : true,
            "call_exprs" : ["foo()"],
            "call_signatures" : [ "void()" ]
          },
          {
            "raw_expr" : "bar",
            "is_callable" : true,
            "call_exprs" : ["bar({{_0}})"],
            "call_signatures" : ["void(int)"]
          }
        ]
      ]
    }
 * 
 */
struct [[helper("array_fn_helper", foo, bar)]] array_fn_struct {};









/*
 *  Will generate this json(foo is_callable false, because the mode is raw_expr and it doesn't support call resolution):
    "helpers" : {
      "array_raw_helper" : [ 
        [
          {
            "raw_expr" : "baz"
            "is_callable" : false
          },
          {
            "raw_expr" : "\"my string literal\"",
            "is_callable" : false
          },
          {
            "raw_expr" : "foo",
            "is_callable" : false
          }
        ]
      ]
    }
 * 
 */
struct [[helper("array_raw_helper", baz, "my string literal", foo)]] array_raw_expr_struct {};











/*
 *  Will generate this json(fn_helper is array in case of multiple definitions):
    "helpers" : {
      "array_mix_helper" : [
        [
          {
            "raw_expr" : "baz"
            "is_callable" : false,
          },
          {
            "raw_expr" : "bar",
            "is_callable" : true,
            "call_exprs" : ["bar({{_0}})"],
            "call_signatures" : ["void(int)"]
          }
        ]
      ]
    }
 * 
 */
struct [[helper("array_mix_helper", baz, bar)]] array_mix_struct {};

}