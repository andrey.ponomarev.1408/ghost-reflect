/*!
 * \file      main.cpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of LLVM project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/LLVM
 */


#include "serialize_interface.hpp"


#include <rapidjson/prettywriter.h>



#include "reflect_class.hpp"

#include <iostream>
int main() {
  using namespace JsonSerialize;
  std::vector<std::string> vec_of_strings = {"foo", "bar", "baz"};
  Document doc;

  StringBuffer buffer;
  PrettyWriter<StringBuffer> pretty(buffer);


  using namespace ghost_reflect_example;


  std::vector<Implementation> structs{
      Implementation(complex_struct(0, {{0.1f, 0.2f}, 0.3f}), 0.4f),
      Implementation(complex_struct(1, {{1.1f, 1.2f}, 1.3f}), 1.4f)
  };
  auto json_vec = Serialize(structs, doc.GetAllocator());
  json_vec.Accept(pretty);
  std::cout << buffer.GetString() << std::endl;
  
  return 0;
}
