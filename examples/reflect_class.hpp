/*!
 * \file      reflect_class.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include "reflect_struct.hpp"


#include <string>

namespace ghost_reflect_example {


class [[reflect]] Base {
public:
  const complex_struct &getVal() const { return val_; }
  void setVal(const complex_struct &val) { val_ = val; }
  void setVal(complex_struct &&val) { val_ = val; }

  virtual void some_interface_function() = 0;
  virtual ~Base() = default;

  Base() = default;
  Base(const complex_struct& v) : val_(v) {}

private:
  [[property(&Base::getVal, &Base::setVal)]]
  complex_struct val_;
};

class [[reflect]] Derived0 : public virtual Base {
public:
  Derived0() = default;
  Derived0(float v) : derived0_value(v) {}
  float derived0_value;
};

class [[reflect]] Derived1 : public virtual Base {
public:
  Derived1() = default;
private:
  std::string derived1_value;
};

class Implementation : public Derived0, public Derived1 {
public:
  Implementation() = default;

  Implementation(const Implementation &) = default;
  Implementation(Implementation &&) = default;

  Implementation &operator=(const Implementation &) = default;
  Implementation &operator=(Implementation &&) = default;

  Implementation(const complex_struct &v0, float v1) : Base(v0), Derived0{v1} {}
  
  virtual void some_interface_function() override {
  }
private:
};

}

/**
  * @author Andrey Ponomarev
  * @brief This file was generated with jinja2_engine made for ghost-reflect,  
 *        available at https://gitlab.com/andrey.ponomarev.1408/ghost-reflect
 * @date August 27, 2019
  
*  
 */

#include <serialize_interface.hpp>
    
namespace JsonSerialize {

namespace description {
template <> struct [[reflect]] [[flag("generated")]] [[flag("serialize", false)]] SerializerImpl<ghost_reflect_example::vec2> {
  void operator()(const ghost_reflect_example::vec2 &obj, Value &json_val,
                  Document::AllocatorType &alloc) {
    json_val.AddMember("x", Serialize(obj.x, alloc), alloc);
    json_val.AddMember("y", Serialize(obj.y, alloc), alloc);
  } // namespace description
};  // namespace JsonSerialize

template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::vec2> {
    void operator()(ghost_reflect_example::vec2 &obj, const Value& val) {
        
        obj.x = Deserialize<float>(val["x"]);
obj.y = Deserialize<float>(val["y"]);
} // namespace description
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::vec3> {
    void operator()(const ghost_reflect_example::vec3 &obj, Value& json_val, Document::AllocatorType &alloc) {
        json_val.AddMember("z", Serialize(obj.z, alloc), alloc);
}
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::vec3> {
    void operator()(ghost_reflect_example::vec3 &obj, const Value& val) {
        
        obj.z = Deserialize<float>(val["z"]);
}
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::complex_struct> {
    void operator()(const ghost_reflect_example::complex_struct &obj, Value& json_val, Document::AllocatorType &alloc) {
        json_val.AddMember("bar", Serialize(obj.getBar(), alloc), alloc);
}
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::complex_struct> {
    void operator()(ghost_reflect_example::complex_struct &obj, const Value& val) {
        
        obj.setBar(Deserialize<ghost_reflect_example::vec3>(val["bar"]));
}
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::Base> {
    void operator()(const ghost_reflect_example::Base &obj, Value& json_val, Document::AllocatorType &alloc) {
        json_val.AddMember("val_", Serialize(obj.getVal(), alloc), alloc);
}
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::Base> {
    void operator()(ghost_reflect_example::Base &obj, const Value& val) {
        
        obj.setVal(Deserialize<ghost_reflect_example::complex_struct>(val["val_"]));
}
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::Derived0> {
    void operator()(const ghost_reflect_example::Derived0 &obj, Value& json_val, Document::AllocatorType &alloc) {
        json_val.AddMember("derived0_value", Serialize(obj.derived0_value, alloc), alloc);
}
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::Derived0> {
    void operator()(ghost_reflect_example::Derived0 &obj, const Value& val) {
        
        obj.derived0_value = Deserialize<float>(val["derived0_value"]);
}
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::Derived1> {
    void operator()(const ghost_reflect_example::Derived1 &obj, Value& json_val, Document::AllocatorType &alloc) {
    }
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::Derived1> {
    void operator()(ghost_reflect_example::Derived1 &obj, const Value& val) {
        
    }
};


template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
SerializerImpl<ghost_reflect_example::Implementation> {
    void operator()(const ghost_reflect_example::Implementation &obj, Value& json_val, Document::AllocatorType &alloc) {
    }
};


template <> 
struct [[reflect]]
[[flag("generated")]] 
[[flag("serialize", false)]]
DeserializerImpl<ghost_reflect_example::Implementation> {
    void operator()(ghost_reflect_example::Implementation &obj, const Value& val) {
        
    }
};



} // namespace description

template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
Serializer<ghost_reflect_example::vec2> {
    Value operator()(const ghost_reflect_example::vec2 &obj, Document::AllocatorType &alloc) {
        
        Value out(kObjectType);
description::SerializerImpl<ghost_reflect_example::vec2>()(obj, out, alloc);
return out;
}
};


template <> 
struct 
[[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
    Deserializer<ghost_reflect_example::vec2> {
        ghost_reflect_example::vec2 operator()(const Value& val) {
            
            ghost_reflect_example::vec2 out;
description::DeserializerImpl<ghost_reflect_example::vec2>()(out, val);
return out;
}
};
template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
Serializer<ghost_reflect_example::vec3> {
    Value operator()(const ghost_reflect_example::vec3 &obj, Document::AllocatorType &alloc) {
        
        Value out(kObjectType);
description::SerializerImpl<ghost_reflect_example::vec2>()(obj, out, alloc);
description::SerializerImpl<ghost_reflect_example::vec3>()(obj, out, alloc);
return out;
}
};


template <> 
struct 
[[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
    Deserializer<ghost_reflect_example::vec3> {
        ghost_reflect_example::vec3 operator()(const Value& val) {
            
            ghost_reflect_example::vec3 out;
description::DeserializerImpl<ghost_reflect_example::vec2>()(out, val);
description::DeserializerImpl<ghost_reflect_example::vec3>()(out, val);
return out;
}
};
template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
Serializer<ghost_reflect_example::complex_struct> {
    Value operator()(const ghost_reflect_example::complex_struct &obj, Document::AllocatorType &alloc) {
        
        Value out(kObjectType);
description::SerializerImpl<ghost_reflect_example::complex_struct>()(obj, out, alloc);
return out;
}
};


template <> 
struct 
[[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
    Deserializer<ghost_reflect_example::complex_struct> {
        ghost_reflect_example::complex_struct operator()(const Value& val) {
            
            ghost_reflect_example::complex_struct out;
description::DeserializerImpl<ghost_reflect_example::complex_struct>()(out, val);
return out;
}
};
template <> 
struct [[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
Serializer<ghost_reflect_example::Implementation> {
    Value operator()(const ghost_reflect_example::Implementation &obj, Document::AllocatorType &alloc) {
        
        Value out(kObjectType);
description::SerializerImpl<ghost_reflect_example::Base>()(obj, out, alloc);
description::SerializerImpl<ghost_reflect_example::Derived0>()(obj, out, alloc);
description::SerializerImpl<ghost_reflect_example::Derived1>()(obj, out, alloc);
description::SerializerImpl<ghost_reflect_example::Implementation>()(obj, out, alloc);
return out;
}
};


template <> 
struct 
[[reflect]] 
[[flag("generated")]] 
[[flag("serialize", false)]]
    Deserializer<ghost_reflect_example::Implementation> {
        ghost_reflect_example::Implementation operator()(const Value& val) {
            
            ghost_reflect_example::Implementation out;
description::DeserializerImpl<ghost_reflect_example::Base>()(out, val);
description::DeserializerImpl<ghost_reflect_example::Derived0>()(out, val);
description::DeserializerImpl<ghost_reflect_example::Derived1>()(out, val);
description::DeserializerImpl<ghost_reflect_example::Implementation>()(out, val);
return out;
}
};

} // namespace JsonSerilize
