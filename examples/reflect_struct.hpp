/*!
 * \file      reflect_struct.hpp
 * \author    Andrey Ponomarev (andrey.ponomarev.1408@gmail.com)
 * \date      2019
 * \copyright This file is part of Ghost-Reflect Project
 *            License is available at
 * https://gitlab.com/andrey.ponomarev.1408/ghostengine
 */

#pragma once

#include <iostream>




namespace [[reflect]] ghost_reflect_example {

  struct [[reflect(false)]] vec2 {
    float x;
    float y;
  };

  void function() {
    struct inside_function {

    };
  }

  template<typename T>
  struct templated_struct {
    struct [[reflect]] nested_struct {
      int a;
    };
  };

  template<>
  struct  templated_struct<int> {
    int i;
    struct [[reflect]] nested_struct {
      int a;
    };
  };

  struct foo : public templated_struct<int> {
    int aaaa;
  };

  struct vec3 : vec2 {
    float z;
  };

  struct complex_struct {
  public:
    complex_struct() = default;
    complex_struct(const complex_struct &) = default;
    complex_struct(complex_struct &&) = default;
    complex_struct &operator=(const complex_struct &) = default;
    complex_struct &operator=(complex_struct &&) = default;
    complex_struct(int foo_, const vec3 &bar_) : foo(foo_), bar(bar_) {}

    int getFoo() const { return foo; }
    
    vec3 getBar() const { return bar; }
    vec3 &getBar() { return bar; }
    void setBar(vec3 val) { bar = val; }
    
  private:
    [[property(&complex_struct::getFoo)]]
    [[flag("serialize", false)]]
    int foo;

    [[property(&complex_struct::getBar, &complex_struct::setBar)]]
    vec3 bar;
  };
  
  struct base {
    
  };
  
  struct derive00 : public virtual base {
    
  };
  
  struct derive01 : public virtual base {
    
  };
  
  struct derive10 : public derive00, public derive01 {
    
  };
}


