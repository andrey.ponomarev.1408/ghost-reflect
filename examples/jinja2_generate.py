import re
import json
import subprocess
import os
import sys, argparse
from datetime import date

from jinja2 import Environment, FileSystemLoader
from subprocess import Popen, PIPE, STDOUT

def call_function(val, *args):
  result = None
  for index, arg in enumerate(args, start=0):
    if result is None:
      result = re.subn('(\\w*)(_'+str(index)+')(\\w*)',r"\1" +arg+ r"\3", val)
    else:
      result = re.subn('(\\w*)(_'+str(index)+')(\\w*)',r"\1" +arg+ r"\3", result[0])

    if result[1] == 0:
      raise RuntimeError("\'" + val + "\' doesn't have argument number " + str(index))
  
  return result[0]

def Traverse(json_obj: dict, template, prefix=""):
  for field, value in json_obj.items():        
    if field == "namespaces":
      for ns in value:
        name = ns["name"]
        Traverse(ns, template, prefix= prefix+name+"::" if name else prefix)
    elif field == "records":
      for record in value:
        result = template.render(prefix=prefix, data_type="record", data=record, env=os.environ, date=date.today().strftime("%B %d, %Y"))
        print(result)
        Traverse(record, template)


def ProccessFile(json_file, template_file):
  templateLoader = FileSystemLoader(searchpath="./", followlinks=True)
  templateEnv = Environment(loader=templateLoader)
  templateEnv.trim_blocks = True
  templateEnv.filters['call_function'] = call_function
  template = templateEnv.get_template(template_file)

  with open(json_file, 'r') as f:
    data = json.load(f)
  
  Traverse(data, template)

  # print(template.render(data=data, env=os.environ, date=date.today().strftime("%B %d, %Y")))


# Main
parser = argparse.ArgumentParser(description='Generates text files from json files and jinja2 template.')
parser.add_argument('template', metavar='template', type=str,
                    help='Jinja2 template file')
parser.add_argument('json_files', metavar='json_files', nargs='+',
                    help='List of json files')

args = parser.parse_args()

for json_file in args.json_files:
  ProccessFile(json_file, args.template)


